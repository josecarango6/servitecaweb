package Entidades;

import Entidades.ServiciosPrestados;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-09T17:04:40")
@StaticMetamodel(Descuento.class)
public class Descuento_ { 

    public static volatile SingularAttribute<Descuento, Integer> porcentajeDescuento;
    public static volatile SingularAttribute<Descuento, Integer> idDESCUENTO;
    public static volatile SingularAttribute<Descuento, Integer> cantidadServicios;
    public static volatile ListAttribute<Descuento, ServiciosPrestados> serviciosPrestadosList;

}