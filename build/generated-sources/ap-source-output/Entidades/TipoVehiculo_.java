package Entidades;

import Entidades.Vehiculo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-09T17:04:40")
@StaticMetamodel(TipoVehiculo.class)
public class TipoVehiculo_ { 

    public static volatile SingularAttribute<TipoVehiculo, String> nombreTipoVehiculo;
    public static volatile SingularAttribute<TipoVehiculo, Integer> idTIPOVEHICULO;
    public static volatile ListAttribute<TipoVehiculo, Vehiculo> vehiculoList;

}