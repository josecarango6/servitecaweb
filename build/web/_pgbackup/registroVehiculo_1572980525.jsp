<%@page import="static Controlador.traetipo.*"%>
<%@page import="modelo.DAO.ServiciosPrestadosDAO"%> 
<%@page import="Dbutil.Mensajes"%> 
<%@page import="static Controlador.registroVehiculo.*"%> 
<%@page import="modelo.DAO.ClienteDAO"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="java.util.List"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="modelo.DAO.TipoVehiculoDAO"%> 
<!doctype html> 
<html lang="en"> 
    <%
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
        if (aviso == 1) {
            Mensajes men = new Mensajes();
            men.mensajealerta(response, "registroVehiculo.jsp", "Los Servicios Fueron Registrados Correctamente, el numero de turno es: " + turno + " ");
            aviso = 0;
        }

    %> 
    <head> 
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <meta name="description" content=""> 
        <meta name="author" content=""> 
        <title>Registro Vehiculo</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <!--clockpicker-->         
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>         
        <script src="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>         
        <link rel="stylesheet" href="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css"> 
        <!--clockpicker-->         
        <link href="bootstrap/css/searchbox.css" rel="stylesheet" type="text/css"/> 
    </head>     
    <body> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item">
                    <a href="index.jsp">Inicio</a>
                </li>                 
                <li class="breadcrumb-item active text-white">Registro Vehiculo </li>
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <%@include file="menu.jsp" %> 
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Registro del Vehiculo</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <div class="row text-left"> 
                        <div class="order-md-1 text-left pr-5 pb-5 pl-5 col-md-12"> 
                            <div class="row"> 
                                <h4 class="col-md-4 mr-auto ml-auto mt-3 mb-3">Datos del Vehiculo</h4> 
                                <h4 class="col-md-4 mr-auto ml-auto mt-3 mb-3">Servicios Requeridos</h4> 
                                <h4 class="col-md-4 mr-auto ml-auto mt-3 mb-3">Costo del Servicio</h4> 
                            </div>                             
                            <form class="container" id="needs-validation" novalidate novalidate="" method="post" action="registroVehiculo"> 
                                <div class="row"> 
                                    <div class="mb-3 col-md-2 ">Tipo de Vehiculo
                                        <script>

                                            document.addEventListener("DOMContentLoaded", function (event) {

                                            select();
                                            if (document.readyState !== "complete") {
                                            localStorage.clear();
                                            }

                                            });
                                            function select() {
                                            var select = document.getElementById("select");
                                            for (var i = 0; i < tipos.length; i++) {
                                            consulta(i);
                                            }
                                            var peticion = localStorage.getItem("pos");
                                            select.selectedIndex = peticion;
                                            consulta(peticion - 1);
                                            darformato();
                                            }
                                            function traeservlet() {
                                            window.frames['ventana_iframe'].document.getElementById('automatico').click();
                                            location.reload();
                                            }

                                        </script>
                                        <br> 
                                        <%                                            List<String> lstTipoVehiculo = new LinkedList();
                                            TipoVehiculoDAO tipo = new TipoVehiculoDAO();
                                            lstTipoVehiculo = tipo.obtenerTodosLosVehiculos();%> 
                                        <select id="select" onchange="for (var i = 0; i < tipos.length; i++) {
                                            consulta(i);
                                            }
                                            ;
                                            darformato();
                                            traeservlet()" class="custom-select d-block w-100" required="" id="tipoVehiculo" name="tipoVehiculo"> 
                                            <option value="" disabled selected="disabled selected">seleccione una opcion</option>                                             
                                            <%
                                                //LLenandose :)
                                                for (int i = 0; i < lstTipoVehiculo.size(); i++) {%> 
                                                <option name="tipos"> 
                                                    <%out.print(lstTipoVehiculo.get(i));%> 
                                                </option>                                                 
                                            <%  }%> 
                                        </select>                                         
                                        <iframe style="display: none" id='frm1' src='traetipo' marginwidth='0' marginheight='0' name='ventana_iframe' scrolling='no' border='0' frameborder='0' width='300' height='200'>
</iframe>                                         
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
</div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-2">Placa&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                        <br> 
                                        <input type="text" class="form-control" id="placa" placeholder="" value="" required="" name="placa"> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>
                                    </div>
                                    <div class="btn-group-vertical col-md-6 col-lg-4" data-toggle="buttons"> 
                                        <%
                                            //prueba
                                            List<String> lstDoc = new LinkedList();
                                            ServicioDAO tiposervicio = new ServicioDAO();
                                            lstDoc = tiposervicio.obtenerTodosLosServicios();//LLenandose :)
                                            for (int i = 0; i < lstDoc.size(); i++) {%> 
                                            <label class="btn btn-secondary" for="<%out.print(lstDoc.get(i));%>"> 
                                                <input type="checkbox" onchange="noseve()" value="<%out.print(lstDoc.get(i));%>" autocomplete="off" d="<%out.print(lstDoc.get(i));%>" name="servicio"> 
                                                <%out.print(lstDoc.get(i));
                                            %> 
                                            </label>                                             
                                        <%}%> 
                                    </div>                                     
                                    <div class="col-md-6 col-lg-4"> 
                                        <%for (int i = 0; i < lstDoc.size(); i++) {%> 
                                            <input class="form-control mb-2 " name='costos' id="costos<%out.print(lstDoc.get(i));%>" placeholder="" value=""> 
                                            <div class="invalid-feedback"> 
</div>                                             
                                        <%}%> 
                                        <script>
                                            function darformato() {//le da formato decimal a los precios
                                            var valores = document.getElementsByName("costos");
                                            for (var i = 0; i < valores.length; i++) {
                                            if (valores[i].value !== "") {
                                            valores[i].value = valores[i].value
                                                    .replace(/\D/g, "")
                                                    .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                                            }
                                            }
                                            }

                                        </script>                                         
                                        <%

                                            ServiciosPrestadosDAO servicostos = new ServiciosPrestadosDAO();
                                            List<String> lstcostos1 = new LinkedList();

                                            if (vehiculocosto != null) {
                                                lstcostos1 = servicostos.selectcostos(vehiculocosto);
                                            }
                                            //Agregando por cadena

                                        %> 
                                    </div>                                     
                                </div>                                 
                                <div class="row">
                                    <div class="mb-3 col-md-2 col-lg-4">Tipo Registro
                                        <script>
                                            function tiposelect() {
                                            var option = document.getElementById("opcion");
                                            var checks = document.getElementById('buttons');
                                            if (option.value === "2") {

                                            checks.style.display = 'block';
                                            } else {
                                            checks.style.display = 'none';
                                            document.getElementById('busqueda').value = "";
                                            }


                                            }
                                        </script>
                                        <select id="opcion" onchange="tiposelect()" lass="custom-select d-block w-100" required="" id="tipoVehiculo" name="tiporegistro"> 
                                            <option value="" disabled selected="disabled selected">seleccione una opcion</option>                                             
                                            <option value="1">Anonimo</option>                                             
                                            <option value="2">Cliente asociado</option>                                             
                                        </select>                                         
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
</div>                                         
                                    </div>                                     
                                    <div id="buttons" class="mb-3 col-md-2 col-lg-4"># Documento
                                        <input type="text" class="form-control" id="busqueda" name="numeroDocumento" value="">
                                    </div>                                     
                                    <!--<div class="invalid-feedback"> 
                                            Valid first name is required.
                                        </div>  -->
                                </div>                                 
                                <div class="row mt-3"> 
                                    <div class="col-md-6 mb-3 col-lg-4 mt-2">Fecha de Ingreso
                                        <br> 
                                        <input type="date" class="form-control" id="fecha" placeholder="ejemplo@ejemplo.com" name="fecha" required> 
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
</div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-4 mt-2">Hora de Ingreso
                                        <br> 
                                        <input type="text" class="form-control" id="hora" name="horaingreso" placeholder="" value="" required=""> 
                                        <script>
                                            $("input[name=horaingreso]").clockpicker({
                                            placement: 'bottom',
                                                    align: 'left',
                                                    autoclose: true,
                                                    default: 'now',
                                                    donetext: "Select"
                                            });
                                        </script>                                         
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-4"> 
                                        <h4 class="m-auto text-center">Costo Total Servicios</h4> 
                                        <input type="text" class="form-control" id="costoServicios" placeholder="" style="display: none"> 
                                        <input type="button" class="form-control" id="costoServicios2" onclick="this.value = resultado;" placeholder="" value="calcular"> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                </div>                                 
                                <script>
                                    //obtener arreglo de costos y arreglo de servicios y arreglo de tipos
                                    var costos = document.getElementsByName("costos");
                                    var servicios = document.getElementsByName("servicio");
                                    var tipos = document.getElementsByName("tipos");
                                    var fd3 = document.getElementsByName("costoServicios2");
                                    var val = [];
                                    var resultado = 0;
                                    function noseve() {

                                    for (var i = 0; i < servicios.length; i++) {
                                    if (!servicios[i].checked) {
                                    costos[i].style.backgroundColor = 'gray';
                                    }

                                    if (servicios[i].checked) {
                                    costos[i].style.backgroundColor = 'white';
                                    }

                                    }
                                    resultado = 0;
                                    for (var i = 0; i < costos.length; i++) {
                                    if (costos[i].style.backgroundColor === 'white') {
                                    var str = costos[i].value;
                                    var array = str.split(".");
                                    var acum = "";
                                    for (var j = 0; j < array.length; j++) {
                                    acum = acum + array[j];
                                    }


                                    resultado = resultado + parseInt(acum);
                                    }

                                    }
                                    resultado = resultado.toString().replace(/\D/g, "")
                                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                                    }

                                    function recorre(w) {
                                    if (tipochange() === w) {
                                    <%for (int i = 0; i < lstcostos1.size(); i++) {%>
                                    val[w] = document.getElementById("" + costos[<%out.print(i);%>].getAttribute('id') + "").value = <%out.print(lstcostos1.get(i));%>;
                                    <%}%>

                                    }



                                    }

                                    function consulta(p) {
                                    var pos = tipochange(tipos);
                                    for (var i = 0; i < tipos.length; i++) {


                                    if (servicios[p].checked) {//consulta
                                    if (pos === i) {
                                    recorre(i);
                                    localStorage.setItem("pos", pos + 1);
                                    break;
                                    }


                                    //alert("tipo de vehiculo " + pos + " servicio " + servicios[i].value + " se inserta en " + costos[i].getAttribute("id"));
                                    } else if (!servicios[p].checked) {
                                    if (pos === i) {
                                    recorre(i);
                                    localStorage.setItem("pos", pos + 1);
                                    break;
                                    }
                                    }

                                    }


                                    }
                                    function tipochange(tiposdevehiculos) {
                                    var opt;
                                    for (var i = 0, len = tipos.length; i < len; i++) {
                                    opt = tipos[i];
                                    if (opt.selected === true) {
                                    break;
                                    }
                                    }
                                    return i;
                                    }
                                </script>                                 
                                <hr class="mb-4"> 
                                <button class="btn btn-lg btn-block mb-5 ml-auto mr-auto col-md-5 col-lg-4 btn-primary" type="submit">Registrar Servicios</button>                                 
                            </form>                             
                            <script src="assets/js/jquery.min.js" script>
                                    <script type="bootstrap/js/bootstrap.min.js" </script>
                            <script>
                            // Example starter JavaScript for disabling form submissions if there are invalid fields
                            (function() {
                                    'use strict';
                            window.addEventListener('load', function() {
                            var form = document.getElementById('needs-validation');
                            form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                    }
                        form.classList.add('was-validated');
                        }, false);
                            }, false);
                            })();
                                </script>                             
                        </div>                         
                    </div>                     
                </main>                 
            </div>             
        </div>         
        <!-- Bootstrap core JavaScript
        ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->         
        <script src="assets/js/jquery.min.js"></script>         
        <script src="assets/js/popper.js"></script>         
        <script src="bootstrap/js/bootstrap.min.js"></script>         
        <!-- Icons -->         
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>         
        <script>feather.replace()</script>         
        <!-- Graphs -->         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>         
        <script>
                                                var ctx = document.getElementById("myChart");
                                                var myChart = new Chart(ctx, {
                                                type: 'line',
                                                        data: {
                                                        labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                                datasets: [{
                                                                data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                                        lineTension: 0,
                                                                        backgroundColor: 'transparent',
                                                                        borderColor: '#007bff',
                                                                        borderWidth: 4,
                                                                        pointBackgroundColor: '#007bff'
                                                }]
                                                },
                                                options: {
                                                                        scales: {
                                                                        yAxes: [{
                                                                        ticks: {
                                                                        beginAtZero: false
                                                }
                                                }]
                                                },
                                                legend: {
                                                                                display: false,
                                                }
                                                }
                                        });
                </script>         
    </body>     
</html>
