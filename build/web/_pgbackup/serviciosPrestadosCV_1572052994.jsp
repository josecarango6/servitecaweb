<%-- 
    Document   : index
    Created on : 8/09/2019, 4:11:44 p. m.
    Author     : jooss
--%> 
<%@page import="modelo.DAO.ReportesDAO"%> 
<%@page import="java.text.DateFormat"%> 
<%@page import="java.sql.Date"%> 
<%@page import="java.text.SimpleDateFormat"%> 
<%@page import="modelo.DAO.ServiciosPrestadosDAO"%> 
<%@page import="java.util.List"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="static Controlador.ReporteServicioCV.*"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!doctype html> 
<html lang="en"> 
    <head> 
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <meta name="description" content=""> 
        <meta name="author" content=""> 
        <title>Dashboard Template for Bootstrap</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css"> 
        <!-- Font Awesome -->         
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"> 
        <!-- Bootstrap core CSS -->         
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"> 
        <!-- Material Design Bootstrap -->         
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.11/css/mdb.min.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>         
    </head>     
                <body> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item"> 
                    <a href="index.jsp">Inicio</a> 
                </li>                 
                <li class="breadcrumb-item active text-white">Servicios Prestados por Cliente o Automotor</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <%            String[] fechas = new String[0];
            String Cadena = "";
            List<String> listafecha = new LinkedList();
            String lista = "";
            int[] idServicios = new int[0];
            if (Servicios != null) {
                idServicios = new int[Servicios.length];
            }

            ReportesDAO rserv = new ReportesDAO();
            try {

                Date dateFecha1 = new Date(System.currentTimeMillis());
                Date dateFecha2 = new Date(System.currentTimeMillis());

                long dias = 0;
                int y = 0;

                if (fecha1 != null && fecha2 != null) {
                    dateFecha1 = Date.valueOf(fecha1);
                    dateFecha2 = Date.valueOf(fecha2);
                    long diferenciaMils = dateFecha2.getTime() - dateFecha1.getTime();

                    //obtenemos los segundos
                    long segundos = diferenciaMils / 1000;

                    //obtenemos las horas
                    long horas = segundos / 3600;
                    dias = horas / 24;//rengo de fechas
                    y = Math.toIntExact(dias) + 1;
                    fechas = new String[y];

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String text = df.format(dateFecha1);
                    for (int i = 0; i < y; i++) {

                        fechas[i] = rserv.sumarDiasFecha2(text, i);

                    }

                }

                ServiciosPrestadosDAO daoServiPresta = new ServiciosPrestadosDAO();

                for (int j = 0; j < Servicios.length; j++) {

                    for (int i = 0; i < y; i++) {

                        idServicios[j] = daoServiPresta.obtenerIdServicio(Servicios[j]);

                    }
                }
            } catch (NullPointerException e) {

            }
        %> 
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky text-justify"> 
                        <ul class="nav flex-column"> 
                            <li class="nav-item"> 
                                <a class="nav-link active" href="index.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"> 
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>                                         
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>                                         
                                    </svg> 
                                    Inicio <span class="sr-only">(current)</span> </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="liquidacion.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"> 
                                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>                                         
                                        <polyline points="13 2 13 9 20 9"></polyline>                                         
                                    </svg> 
                                    Liquidación </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="registroVehiculo.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 11 11"> 
                                        <path d="M9 4l-.89-2.66A.5.5 0 0 0 7.64 1H3.36a.5.5 0 0 0-.47.34L2 4a1 1 0 0 0-1 1v3h1v1a1 1 0 1 0 2 0V8h3v1a1 1 0 1 0 2 0V8h1V5a1 1 0 0 0-1-1zM3 7a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm0-3l.62-2h3.76L8 4H3zm5 3a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" fill="#626262"></path>                                         
                                    </svg> 
                                    Registro Vehiculo </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="registroCliente.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"> 
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>                                         
                                        <circle cx="9" cy="7" r="4"></circle>                                         
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>                                         
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>                                         
                                    </svg>                                Registro Cliente </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="servicios.jsp"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 20 20"> 
                                        <path d="M4 5H.78c-.37 0-.74.32-.69.84l1.56 9.99S3.5 8.47 3.86 6.7c.11-.53.61-.7.98-.7H10s-.7-2.08-.77-2.31C9.11 3.25 8.89 3 8.45 3H5.14c-.36 0-.7.23-.8.64C4.25 4.04 4 5 4 5zm4.88 0h-4s.42-1 .87-1h2.13c.48 0 1 1 1 1zM2.67 16.25c-.31.47-.76.75-1.26.75h15.73c.54 0 .92-.31 1.03-.83.44-2.19 1.68-8.44 1.68-8.44.07-.5-.3-.73-.62-.73H16V5.53c0-.16-.26-.53-.66-.53h-3.76c-.52 0-.87.58-.87.58L10 7H5.59c-.32 0-.63.19-.69.5 0 0-1.59 6.7-1.72 7.33-.07.37-.22.99-.51 1.42zM15.38 7H11s.58-1 1.13-1h2.29c.71 0 .96 1 .96 1z" fill="#626262"></path>                                         
                                    </svg> 
                                    Servicios </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="descuento.jsp"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 1024 1024"> 
                                        <path d="M855.7 210.8l-42.4-42.4a8.03 8.03 0 0 0-11.3 0L168.3 801.9a8.03 8.03 0 0 0 0 11.3l42.4 42.4c3.1 3.1 8.2 3.1 11.3 0L855.6 222c3.2-3 3.2-8.1.1-11.2zM304 448c79.4 0 144-64.6 144-144s-64.6-144-144-144-144 64.6-144 144 64.6 144 144 144zm0-216c39.7 0 72 32.3 72 72s-32.3 72-72 72-72-32.3-72-72 32.3-72 72-72zm416 344c-79.4 0-144 64.6-144 144s64.6 144 144 144 144-64.6 144-144-64.6-144-144-144zm0 216c-39.7 0-72-32.3-72-72s32.3-72 72-72 72 32.3 72 72-32.3 72-72 72z" fill="#626262"></path>                                         
                                    </svg> 
                                    Descuentos</a> 
                                <a class="nav-link" href="turno.jsp"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 24 24"> 
                                        <path opacity=".3" d="M5 19h14V5H5v14zm2.41-7.41L10 14.17l6.59-6.59L18 9l-8 8l-4-4l1.41-1.41z" fill="#626262"/> 
                                        <path d="M18 9l-1.41-1.42L10 14.17l-2.59-2.58L6 13l4 4zm1-6h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04a2.008 2.008 0 0 0-1.44 1.19c-.1.24-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55c.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75s-.75-.34-.75-.75s.34-.75.75-.75zM19 19H5V5h14v14z" fill="#626262"/> 
                                    </svg> 
                                    Turno</a> 
                            </li>                             
                        </ul>                         
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted"> <span class="text-primary">Reportes</span> <a class="d-flex align-items-center text-muted" href="#"> </a> </h6> 
                        <ul class="nav flex-column mb-2"> 
                            <li class="nav-item"> 
                                <a class="nav-link text-left" href="serviciosPrestados.jsp"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"> 
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>                                         
                                        <polyline points="14 2 14 8 20 8"></polyline>                                         
                                        <line x1="16" y1="13" x2="8" y2="13"></line>                                         
                                        <line x1="16" y1="17" x2="8" y2="17"></line>                                         
                                        <polyline points="10 9 9 9 8 9"></polyline>                                         
                                    </svg> 
                                    Cantidad de Servicios Prestados y Tiempo de Atención </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link text-left" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"> 
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>                                         
                                        <polyline points="14 2 14 8 20 8"></polyline>                                         
                                        <line x1="16" y1="13" x2="8" y2="13"></line>                                         
                                        <line x1="16" y1="17" x2="8" y2="17"></line>                                         
                                        <polyline points="10 9 9 9 8 9"></polyline>                                         
                                    </svg>&nbsp;Servicios Prestados por Vehiculo o Automotor</a> 
                            </li>                             
                            <li class="nav-item"> 
</li>                             
                            <li class="nav-item"> 
</li>                             
                        </ul>                         
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Servicios Prestados por Cliente o Automotor</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <div class="order-md-1 text-left bg-white pr-5 pb-5 pl-5 col-md-12"> 
                        <h4 class="mb-3 mt-auto">Reporte de Servicios Prestados</h4> 
                        <form class="needs-validation" novalidate="" method="post" action="ReporteServicioCV"> 
                            <div class="row"> 
                                <div class="col-md-6 mb-3 mt-2 col-lg-4">Seleccione el Tipo de Reporte
                                    <br> 
                                    <script>

                                        function tiposelect() {
                                            var option = document.getElementById("tReporte");
                                            var checks = document.getElementById('buttons1');
                                            var checks2 = document.getElementById('buttons2');


                                            if (option.value === "1") {

                                                checks.style.display = 'block';
                                            } else {
                                                checks.style.display = 'none';
                                            }
                                            
                                            if (option.value === "2") {

                                                checks2.style.display = 'block';
                                            } else {
                                                checks2.style.display = 'none';
                                            }


                                        }

                                    </script>                                     
                                    <select id="tReporte" onchange="tiposelect()" class="custom-select" name="tReporte"> 
                                        <option selected>Seleccione una opción</option>                                         
                                        <option value="1">Cantidad de Servicios por Cliente</option>                                         
                                        <option value="2">Cantidad de Servicios por Automotor</option>                                         
                                    </select>                                     
                                    <div class="invalid-feedback"> 
                                        Valid first name is required.
</div>                                     
                                </div>                                 
                                <div data-toggle="buttons" class="btn-group-vertical col-lg-3"> Selecione los Servicios
                                    <%                                           //prueba
                                        List<String> lstDoc = new LinkedList();
                                        ServicioDAO tiposervicio = new ServicioDAO();
                                        lstDoc = tiposervicio.obtenerTodosLosServicios();//LLenandose :)
                                        for (int i = 0; i < lstDoc.size(); i++) {%> 
                                        <label class="btn btn-secondary" for="<%out.print(lstDoc.get(i));%>"> 
                                            <input type="checkbox" value="<%out.print(lstDoc.get(i));%>" autocomplete="off" d="<%out.print(lstDoc.get(i));%>" name="servicio"> 
                                            <%out.print(lstDoc.get(i));
                                        %> 
                                        </label>                                         
                                    <%}%> 
                                </div>                                 
                                <div id="buttons1" class="mb-3 col-md-3 col-lg-2 mt-2">Seleccione el documento
                                    <div class="container-fluid"> 
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <select class="SelectDocumento" style="width: 100%"> 
</select>                                                 
                                            </div>                                             
                                        </div>                                         
                                    </div>                                     
                                    <script>
                                        var data = [];
                                       <% List<String> listaDocumentos = new LinkedList();
                                        ServicioDAO documentos = new ServicioDAO();
                                        listaDocumentos = documentos.obtenerDocumentos(); 
              
                                            for (int i = 0; i < listaDocumentos.size(); i++) {%> 
                                                data[<%out.print(i);%>]="<%out.print(listaDocumentos.get(i));%>";
                                                <%}%>
                                                
                                        var placeholder = "select";
                                        $(".SelectDocumento").select2({
                                            data: data,
                                            placeholder: placeholder,
                                            allowClear: false,
                                            minimumResultsForSearch: 1
                                        });
                                    </script>                                     
                                    <br> 
                                </div>
                                <div id="buttons2" class="mb-3 col-md-3 col-lg-2 mt-2">Seleccione la placa
                                    <div class="container-fluid"> 
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <select class="SelectPlaca" style="width: 100%"> 
</select>                                                 
                                            </div>                                             
                                        </div>                                         
                                    </div>                                     
                                    <script>
                                        var data = [];
                                       <% List<String> listaPlacas = new LinkedList();
                                        ServicioDAO placas = new ServicioDAO();
                                        listaPlacas = placas.obtenerPlacas(); 
              
                                            for (int i = 0; i < listaPlacas.size(); i++) {%> 
                                                data[<%out.print(i);%>]="<%out.print(listaPlacas.get(i));%>";
                                                <%}%>
                                                
                                        var placeholder = "select";
                                        $(".SelectPlaca").select2({
                                            data: data,
                                            placeholder: placeholder,
                                            allowClear: false,
                                            minimumResultsForSearch: 1
                                        });
                                    </script>                                     
                                    <br> 
                                </div>                                 
                            </div>                             
                            <div class="invalid-feedback"> 
                                Valid first name is required.
</div>                                                          
                    </div>                     
                    <div class="invalid-feedback"> 
                        Valid first name is required.
</div>                     
            </div>             
            <div class="row"> 
</div>             
            <hr class="mb-4"> 
            <div class="row mb-5"> 
                <button class="btn btn-primary btn-lg btn-block m-auto col-md-4" type="submit">Consultar</button>                 
            </div>             
        </form>         
        <table style="width: 100px;float: left" class="table"> 
            <thead> 
                <tr> 
                    <th style="height: 80px">Fechas</th> 
                </tr>                 
            </thead>             
                <tbody> 
                <%for (int i = 0; i < fechas.length; i++) {%> 
                <tr> 
                        <td><%out.print(fechas[i]);%></td> 
                    <% }
                                %> 
                    </tr>                     
            </tbody>             
        </table>         
        <%if (tiporeporte != null) {%> 
            <%if (tiporeporte.equals("1")) {
                            Cadena = documento;
                        }
                        if (tiporeporte.equals("2")) {
                            Cadena = placa;
                        }
                    %> 
            <%int pos = 0;%> 
            <%if (Servicios != null) {%> 
                <%for (int w = 0; w < Servicios.length; w++) {%> 
                    <table style="width: 100px;float: left" class="table"> 
                        <thead> 
                                <tr> 
                                <%--<th class="fechasocultas" width="10%">Fecha</th>--%> 
                                <%if (Servicios != null) {%> 
                                <th style="height: 80px"><%out.print(Servicios[w]);
                                    }%></th> 
                            </tr>                             
                        </thead>                         
                        <tbody> 
                            <%for (int i = 0; i < fechas.length; i++) {%> 
                                <tr> 
                                    <%--<td class="fechasocultas" width="10%"><%out.print(fechas[i]);%> 
                                </td>                                                                                                                                                                                                                                                                            --%>
                                <%
                                    if (tiporeporte.equals("1")) {
                                        lista = rserv.ReporteCantidadxNumdocuento(idServicios[w], fechas[i], Cadena);
                                    }
                                    if (tiporeporte.equals("2")) {

                                        lista = rserv.ReporteCantidadxplaca(idServicios[w], fechas[i], Cadena);
                                    }

                                    listafecha.add(lista);%> 
                                <%for (int j = pos; j < listafecha.size(); j++) {%> 
                                    <td width="5%"><%out.print(listafecha.get(i + j));%></td> 
                                    <%if (i == fechas.length - 1) {
                                        pos = listafecha.size() - 1 / Servicios.length;
                                    }%> 
                                <%break;
                                    }%> 
                            <%}%> 
                        </tr>                         
                    </tbody>                     
    </table>     
    <%}
                        }%> 
    <%
                        }%> 
</div>   
<!-- Bootstrap core JavaScript
            ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="assets/js/jquery.min.js"></script> 
<script src="assets/js/popper.js"></script> 
<script src="bootstrap/js/bootstrap.min.js"></script> 
<!-- Icons --> 
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script> 
<script>feather.replace()</script> 
<!-- Graphs --> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script> 
<script>
                                        var ctx = document.getElementById("myChart");
                                        var myChart = new Chart(ctx, {
                                            type: 'line',
                                            data: {
                                                labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                datasets: [{
                                                        data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                        lineTension: 0,
                                                        backgroundColor: 'transparent',
                                                        borderColor: '#007bff',
                                                        borderWidth: 4,
                                                        pointBackgroundColor: '#007bff'
                                                    }]
                                            },
                                            options: {
                                                scales: {
                                                    yAxes: [{
                                                            ticks: {
                                                                beginAtZero: false
                                                            }
                                                        }]
                                                },
                                                legend: {
                                                    display: false,
                                                }
                                            }
                                        });
    </script>
