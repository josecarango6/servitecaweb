// Generated by Selenium IDE
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
public class TextRegistroClienteTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void textRegistroCliente() {
    driver.get("http://localhost:8080/Serviteca/login.jsp");
    driver.manage().window().setSize(new Dimension(1296, 696));
    driver.findElement(By.id("inputEmail")).sendKeys("juan");
    driver.findElement(By.id("inputPassword")).click();
    driver.findElement(By.id("inputPassword")).sendKeys("1234");
    driver.findElement(By.name("login")).click();
    driver.findElement(By.linkText("Registro Cliente")).click();
    driver.findElement(By.id("tipoDocumento")).click();
    {
      WebElement dropdown = driver.findElement(By.id("tipoDocumento"));
      dropdown.findElement(By.xpath("//option[. = 'Cedula de Ciudadania']")).click();
    }
    driver.findElement(By.id("tipoDocumento")).click();
    driver.findElement(By.id("myInput")).click();
    driver.findElement(By.id("myInput")).sendKeys("1037656789");
    driver.findElement(By.id("validationCustom03")).click();
    driver.findElement(By.id("validationCustom03")).sendKeys("Pedro");
    driver.findElement(By.id("segundoNombre")).sendKeys("Juan");
    driver.findElement(By.id("primerApellido")).sendKeys("Gonzalez");
    driver.findElement(By.id("segundoApellido")).click();
    driver.findElement(By.id("segundoApellido")).sendKeys("Giraldo");
    driver.findElement(By.id("email")).click();
    driver.findElement(By.id("email")).sendKeys("pedro@gmail.com");
    driver.findElement(By.id("telefono")).sendKeys("123987");
    driver.findElement(By.id("address")).sendKeys("carrera 78 sur 33");
    driver.findElement(By.id("submit")).click();
  }
}
