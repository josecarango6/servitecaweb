/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PageObjects;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jooss
 */
public class consultas {

    public void timeOut() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void permitirEliminarBD(){
        try {
            try {
                Connection conexion = Database.getConexion();
                String sql = "SET SQL_SAFE_UPDATES = 0";

                PreparedStatement ps = conexion.prepareStatement(sql);

                ps.executeUpdate();
                ps.close();

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(consultas.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
