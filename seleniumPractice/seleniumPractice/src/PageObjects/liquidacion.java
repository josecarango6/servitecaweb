/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author jooss
 */
public class liquidacion {
    
    WebDriver driver;
    By Liquidacion = By.linkText("Liquidación");
    By placa = By.id("placa");
    By turno = By.id("turno");
    By consultar = By.id("consultare");
    By fechaSalida = By.id("fechaSalida");
    By horaSalida = By.id("horaSalida");
    By estadoLiquidacion = By.id("estadoLiquidacion");
    By generarLiquidacion = By.id("generarLiquidacion");
    By aceptar = By.cssSelector(".swal2-confirm");
     By registroExitoso = By.id("aviso");
    public liquidacion(WebDriver driver) {
        
        this.driver = driver;
        
    }

    //clicK en liquidacion
    public void clickLiquidacion() {
        
        driver.findElement(Liquidacion).click();
        
    }

    // digitamos la placa
    public void setPlaca(String strplaca) {
        
        driver.findElement(placa).sendKeys(strplaca);
        
    }

    // digitamos el turno
    public void setTurno(String strturno) {
        
        driver.findElement(turno).sendKeys(strturno);
        
    }

    //clicK en cunsultar
    public void clickConsultar() {
        
        driver.findElement(consultar).click();
        
    }

    // digitamos la fecha de salida
    public void setFechaSalida(String strfechaSalida) {
        
        driver.findElement(fechaSalida).sendKeys(strfechaSalida);
        
    }

    // digitamos la hora de salida
    public void setHoraSalida(String strhoraSalida) {
        
        driver.findElement(horaSalida).sendKeys(strhoraSalida);
        
    }

    // digitamos el estado de la liquidación
    public void setEstadoLiquidacion(String strestadoLiquidacion) {
        
        driver.findElement(estadoLiquidacion).sendKeys(strestadoLiquidacion);
        
    }

    //clicK en registro Cliente
    public void clickGenerarLiquidacion() {
        
        driver.findElement(generarLiquidacion).click();
        
    }
    
     //clicK en registro Cliente
    public void clickAceptar() {
        
        driver.findElement(aceptar).click();
        
    }
    
    public void consultarLiquidacionTest(String placa, String turno) {

        //digitamos la placa       
        this.setPlaca(placa);

        //digitamos el turno
        this.setTurno(turno);

        //click en consultar
        this.clickConsultar();        
        
    }
    
    public void generarLiquidacionTest(String fechaSalida, String horaSalida, String estadoLiquidacion) {

        // digitamos la fecha de salida
        this.setFechaSalida(fechaSalida);

        //digitamos la hora de salida
        this.setHoraSalida(horaSalida);

        // digitamos el estado de la liquidacion
        this.setEstadoLiquidacion(estadoLiquidacion);
        
    }
    
     public String RegistroExitoso() {
        return driver.findElement(registroExitoso).getAttribute("innerText");
    }

    
}
