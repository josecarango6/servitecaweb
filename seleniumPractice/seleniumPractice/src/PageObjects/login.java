/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
/**
 *
 * @author sala304
 */
public class login {
    WebDriver driver;
    By userName = By.name("usuario");
    By password = By.name("clave");
    By signIn = By.name("login");    
 
    
    public login(WebDriver driver){

        this.driver = driver;

    }
    
    //Set user name in textbox

    public void setUserName(String strUserName){

        driver.findElement(userName).sendKeys(strUserName);

    }

    

    //Set password in password textbox

    public void setPassword(String strPassword){

         driver.findElement(password).sendKeys(strPassword);

    }

    

    //Click on login button

    public void clickLogin(){

            driver.findElement(signIn).click();

    }    
    public void loginApplication(String userName, String password){

        //Fill user name

        this.setUserName(userName);

        //Fill password

        this.setPassword(password);

        //Click Login button

        this.clickLogin();          
    }
    
    
}
