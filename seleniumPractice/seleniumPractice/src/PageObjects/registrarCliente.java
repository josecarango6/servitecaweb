/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PageObjects;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jooss
 */
public class registrarCliente {

    WebDriver driver;
    By RegistroCliente = By.linkText("Registro Cliente");
    By tipoDocumento = By.id("tipoDocumento");
    By numeroDocumento = By.id("numeroDocumento");
    By primerNombre = By.id("primerNombre");
    By segundoNombre = By.id("segundoNombre");
    By primerApellido = By.id("primerApellido");
    By segundoApellido = By.id("segundoApellido");
    By email = By.id("email");
    By telefono = By.id("telefono");
    By address = By.id("address");
    By registrar = By.id("registrar");

    public registrarCliente(WebDriver driver) {

        this.driver = driver;

    }

    //clicK en registro Cliente
    public void clickRegistroCliente() {

        driver.findElement(RegistroCliente).click();

    }

    //seleccionamos el tipo de documento
    public void setTipoDocumento() {

        driver.findElement(tipoDocumento).click();
        {
            WebElement dropdown = driver.findElement(By.id("tipoDocumento"));
            dropdown.findElement(By.xpath("//option[. = 'Cedula de Ciudadania']")).click();
        }

    }

    // digitamos el numero de documento
    public void setNumeroDocumento(String strnumeroDocumento) {

        driver.findElement(numeroDocumento).sendKeys(strnumeroDocumento);

    }

    // digitamos el primer nombre
    public void setPrimerNombre(String strprimerNombre) {

        driver.findElement(primerNombre).sendKeys(strprimerNombre);

    }

    // digitamos el segundo nombre
    public void setSegundoNombre(String strsegundoNombre) {

        driver.findElement(segundoNombre).sendKeys(strsegundoNombre);

    }

    // digitamos el primer Apellido
    public void setPrimerApellido(String strprimerApellido) {

        driver.findElement(primerApellido).sendKeys(strprimerApellido);

    }

    // digitamos el segundo Apellido
    public void setSegundoApellido(String strsegundoApellido) {

        driver.findElement(segundoApellido).sendKeys(strsegundoApellido);

    }

    // digitamos el email
    public void setEmail(String stremail) {

        driver.findElement(email).sendKeys(stremail);

    }

    // digitamos el telefono
    public void setTelefono(String strtelefono) {

        driver.findElement(telefono).sendKeys(strtelefono);

    }

    // digitamos el address
    public void setAddress(String straddress) {

        driver.findElement(address).sendKeys(straddress);

    }
    //clicK en registro Cliente

    public void clickRegistrar() {

        driver.findElement(registrar).click();

    }

    public void registrarClienteTest(String numeroDocumento, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String email, String telefono, String address) {

//        //Clic en Registrar Cliente
//        this.clickRegistroCliente();

        //Seleccionar tipo documento        
        this.setTipoDocumento();

        //digitamos numero documento
        this.setNumeroDocumento(numeroDocumento);

        //digitamos el primer nombre
        this.setPrimerNombre(primerNombre);

        //digitar el segundo nombre
        this.setSegundoNombre(segundoNombre);

        //digitamos el primer apellido
        this.setPrimerApellido(primerApellido);

        //digitamos el segundo apellido
        this.setSegundoApellido(segundoApellido);

        //digitamos el email
        this.setEmail(email);

        //digitamos el telefono
        this.setTelefono(telefono);

        //digitamos la dirección
        this.setAddress(address);

    }

    public int comprobarRegistroCliente() {
        int cedula = 0;
        try {
            try {
                Connection conexion = Database.getConexion();
                Statement stmt = conexion.createStatement();
                String sql = "select numeroDocumento from cliente where numeroDocumento = 1098765654";
                ResultSet rs = stmt.executeQuery(sql);

                if (rs.next()) {
                    cedula = rs.getInt(1);
                }

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registrarCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cedula;
    }

    public void eliminarClientePrueba() {
        
        try {
            try {
                Connection conexion = Database.getConexion();
                String sql = "delete from cliente where numeroDocumento = 1098765654";

                PreparedStatement ps = conexion.prepareStatement(sql);

                ps.executeUpdate();
                ps.close();

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registrarCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
