/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PageObjects;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author jooss
 */
public class registrarVehiculo {

    WebDriver driver;
    By RegistroVehículo = By.linkText("Registro Vehículo");
    By tipoVehiculo = By.id("select");
    By placa = By.id("placa");
    By alineacion = By.cssSelector(".btn-secondary:nth-child(1)");
    By balanceo = By.cssSelector(".btn-secondary:nth-child(2)");
    By cambioAceite = By.cssSelector(".btn:nth-child(3)");
    By tiporeporte = By.id("tiporeporte");
    // By documento = By.name("documento");
    By numeroDocumento = By.name("documento");
    By fecha = By.id("fecha");
    By hora = By.id("hora");
    By salirHora = By.id("needs-validation");
    By submitvehiculo = By.name("submitvehiculo");
    By registroExitoso = By.id("aviso");

    public registrarVehiculo(WebDriver driver) {

        this.driver = driver;

    }

    //clicK en Registro Vehículo
    public void clickRegistroVehículo() {

        driver.findElement(RegistroVehículo).click();

    }

    //seleccionamos el tipo de vehiculo
    public void setTipoVehiculo(String strtipoVehiculo) {

        driver.findElement(tipoVehiculo).sendKeys(strtipoVehiculo);

    }

    // digitamos la placa
    public void setPlaca(String strplaca) {

        driver.findElement(placa).sendKeys(strplaca);

    }

    //clicK en alineacion
    public void clickAlineacion() {

        driver.findElement(alineacion).click();

    }

    //clicK en balanceo
    public void clickBalanceo() {

        driver.findElement(balanceo).click();

    }

    //clicK en cambioAceite
    public void clickCambioAceite() {

        driver.findElement(cambioAceite).click();

    }

    //clicK en tiporeporte
    public void setTiporeporte(String strtiporeporte) {

        driver.findElement(tiporeporte).sendKeys(strtiporeporte);

    }

//    //clicK en documento
//    public void clickdocumento() {
//
//        driver.findElement(documento).click();
//
//    }
    // digitamos el numero de documento
    public void setDocumento(String strnumeroDocumento) {

        driver.findElement(numeroDocumento).sendKeys(strnumeroDocumento);

    }

    // digitamos la fecha
    public void setFecha(String strfecha) {

        driver.findElement(fecha).sendKeys(strfecha);

    }

    // digitamos la hora
    public void setHora(String strhora) {

        driver.findElement(hora).sendKeys(strhora);

    }

    public void clicksalirHora() {
        driver.findElement(salirHora).click();

    }

    //clicK en Registrar servicios
    public void clickSubmitVehiculo() {

        driver.findElement(submitvehiculo).click();

    }

    public void registrarVehiculoTest(String tipoVehiculo, String placa, String tiporeporte, String numeroDocumento, String fecha, String hora) {

        //Seleccionar tipo vehiculo        
        this.setTipoVehiculo(tipoVehiculo);

        //digitamos la placa
        this.setPlaca(placa);

        //click en alineacion
        this.clickAlineacion();

        //click en balanceo
        this.clickBalanceo();

        //click en cambio de Aceite
        this.clickCambioAceite();

        //click en tipo reporte
        this.setTiporeporte(tiporeporte);

//        //click en documento
//        this.clickdocumento();
        //enviamos el documento
        this.setDocumento(numeroDocumento);

        // Escribimos la fecha
        this.setFecha(fecha);

        // Escribimos la hora
        this.setHora(hora);

        //salir del focus de la hora
        this.clicksalirHora();

    }

    public String RegistroExitoso() {
        return driver.findElement(registroExitoso).getAttribute("innerText");
    }

    public String comprobarRegistroVehiculo() {
        String placaR = "0";
        try {
            try {
                Connection conexion = Database.getConexion();
                Statement stmt = conexion.createStatement();
                String sql = "select placa from vehiculo where placa = \"YYY999\"";
                ResultSet rs = stmt.executeQuery(sql);

                if (rs.next()) {
                    placaR = rs.getNString(1);
                }

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registrarVehiculo.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarVehiculo.class.getName()).log(Level.SEVERE, null, ex);
        }

        return placaR;
    }

    public int UltimoTurno() {
        int turno = 0;
        try {
            try {
                Connection conexion = Database.getConexion();
                Statement stmt = conexion.createStatement();
                String sql = "select turno from servicios_prestados where idservicios_prestados=(select max(idSERVICIOS_PRESTADOS) from servicios_prestados)";
                ResultSet rs = stmt.executeQuery(sql);

                if (rs.next()) {
                    turno = rs.getInt(1);
                }

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registrarVehiculo.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarVehiculo.class.getName()).log(Level.SEVERE, null, ex);
        }

        return turno;
    }

    public void eliminarServiciosPrueba() {

        try {
            try {
                Connection conexion = Database.getConexion();
                String sql = "DELETE servicios_prestados FROM vehiculo"
                        + "    left JOIN "
                        + "    servicios_prestados ON servicios_prestados.FK_idVEHICULO = vehiculo.idVEHICULO "
                        + "WHERE "
                        + "    vehiculo.placa =\"YYY999\"";

                PreparedStatement ps = conexion.prepareStatement(sql);

                ps.executeUpdate();
                ps.close();

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registrarCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void eliminarVehiculoPrueba() {

        try {
            try {
                Connection conexion = Database.getConexion();
                String sql = "DELETE servicios_prestados, vehiculo FROM vehiculo "
                        + "    left JOIN "
                        + "    servicios_prestados ON servicios_prestados.FK_idVEHICULO = vehiculo.idVEHICULO "
                        + "WHERE "
                        + "    vehiculo.placa =\"YYY999\"";

                PreparedStatement ps = conexion.prepareStatement(sql);

                ps.executeUpdate();
                ps.close();

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registrarCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
