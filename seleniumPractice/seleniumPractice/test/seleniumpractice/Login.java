/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seleniumpractice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObjects.login;
import PageObjects.FlightDetails;
import PageObjects.consultas;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 *
 * @author sala304
 */
public class Login {

    private static WebDriver driver = null;
    login log;
    FlightDetails flight;
    consultas tiempoespera;

    public Login() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        driver.get("http://localhost:8080/Serviteca/login.jsp");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Test of main method, of class SeleniumPractice.
     */
    @Test
    public void testMain() {
        tiempoespera = new consultas();
        String userName = "juan";
        String password = "1234";
        log = new login(driver);
        flight = new FlightDetails(driver);
        log.loginApplication(userName, password);
        assertEquals("Inicio", flight.getIndexValue());
        tiempoespera.timeOut();
    }

}
