/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seleniumpractice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;

import PageObjects.login;
import PageObjects.FlightDetails;
import PageObjects.registrarCliente;
import PageObjects.consultas;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 *
 * @author jooss
 */
public class RegistrarCliente {

    private static WebDriver driver = null;
    login log;
    FlightDetails flight;
    registrarCliente regcliente;
    consultas tiempoespera;

    public RegistrarCliente() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        driver.get("http://localhost:8080/Serviteca/login.jsp");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Test of main method, of class SeleniumPractice.
     */
    @Test
    public void testMain2() {
        log = new login(driver);
        regcliente = new registrarCliente(driver);
        tiempoespera = new consultas();
        String userName = "juan";
        String password = "1234";
        String numeroDocumento = "1098765654";
        String primerNombre = "Juan";
        String segundoNombre = "Carlos";
        String primerApellido = "Lopez";
        String segundoApellido = "Gomez";
        String email = "jcarlos@gmail.com";
        String telefono = "3498768";
        String address = "Carrera 59 sur 98";

        log.loginApplication(userName, password);
        tiempoespera.timeOut();
        regcliente.clickRegistroCliente();
        tiempoespera.timeOut();
        regcliente.registrarClienteTest(numeroDocumento, primerNombre, segundoNombre, primerApellido, segundoApellido, email, telefono, address);
        tiempoespera.timeOut();
        regcliente.clickRegistrar();
        int cedula = regcliente.comprobarRegistroCliente();
        assertEquals(1098765654, cedula);
        tiempoespera.permitirEliminarBD();
        regcliente.eliminarClientePrueba();

    }

}
