/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seleniumpractice;

import PageObjects.consultas;
import PageObjects.FlightDetails;
import PageObjects.login;
import PageObjects.registrarCliente;
import PageObjects.registrarVehiculo;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 *
 * @author jooss
 */
public class RegistrarVehiculo {

    private static WebDriver driver = null;
    login log;
    FlightDetails flight;
    registrarCliente regcliente;
    registrarVehiculo regvehiculo;
    consultas tiempoespera;

    public RegistrarVehiculo() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);

        //driver.manage().Window.maximeze();
        //driver.manage().window().setSize(new Dimension(1296, 696));       
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        driver.get("http://localhost:8080/Serviteca/login.jsp");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Test of main method, of class SeleniumPractice.
     */
    @Test
    public void testMain3() {        
        log = new login(driver);
        regvehiculo = new registrarVehiculo(driver);
        tiempoespera = new consultas();
        int turno = regvehiculo.UltimoTurno();
        turno = turno +1;
        String userName = "juan";
        String password = "1234";
        String tipoVehiculo = "Automovil";
        String placa = "YYY999";
        String tiporeporte = "Clente Registrado";
        String numeroDocumento = "1037656789";
        String fecha = "25-11-2019";
        String hora = "21:15";

        log.loginApplication(userName, password);
        tiempoespera.timeOut();
        regvehiculo.clickRegistroVehículo();
        tiempoespera.timeOut();
        regvehiculo.registrarVehiculoTest(tipoVehiculo, placa, tiporeporte, numeroDocumento, fecha, hora);
        //tiempoespera.timeOut();
        regvehiculo.clickSubmitVehiculo();
        tiempoespera.timeOut();
        assertEquals("Los Servicios Fueron Registrados Correctamente, el numero de turno es: "+turno+""  , regvehiculo.RegistroExitoso());
        driver.findElement(By.cssSelector(".swal2-close")).click();
        String placaR = regvehiculo.comprobarRegistroVehiculo();
        assertEquals("YYY999", placaR);
        tiempoespera.permitirEliminarBD();
        regvehiculo.eliminarServiciosPrueba();
        regvehiculo.eliminarVehiculoPrueba();
//        
    }

}
