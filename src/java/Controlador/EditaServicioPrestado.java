package Controlador;

import Entidades.ServiciosPrestados;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.ServiciosPrestadosDAO;

@WebServlet(name = "EditaServicioPrestado", urlPatterns = {"/EditaServicioPrestado"})
public class EditaServicioPrestado extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            String[] nombreservicios = request.getParameterValues("servicio");

            ServiciosPrestadosDAO SVD = new ServiciosPrestadosDAO();
            int marcadoschecks = nombreservicios.length;
            String turno = request.getParameter("turno");
            String placa = request.getParameter("placa");
            int cantidad = SVD.totalservipresta(Integer.parseInt(turno), placa);
            if (cantidad == marcadoschecks) {
                //update ciclo normal
                //vector del mismo tamaño 
                int[] idServicios = new int[nombreservicios.length];

                //lista fk servicios viejos
                List<Integer> listaserv_viejo = new LinkedList<>();
                listaserv_viejo = SVD.conFkservicios(Integer.parseInt(turno), placa);

                for (int i = 0; i < nombreservicios.length; i++) {
                    idServicios[i] = SVD.obtenerIdServicio(nombreservicios[i]);
//amedida que lo obtiene va insertando
                    SVD.modificar(idServicios[i], listaserv_viejo.get(i), Integer.parseInt(turno), placa);//aca va el metodo update
//vector de objetos insertando

                }
                SVD.callProcedureServi();
            }
            if (cantidad > marcadoschecks) {
                //update ciclo normal se borran los que no estuvieron en el ciclo udpate               
                int[] idServicios = new int[nombreservicios.length];
                //lista fk servicios viejos
                List<Integer> listaserv_viejo = new LinkedList<>();
                listaserv_viejo = SVD.conFkservicios(Integer.parseInt(turno), placa);
                int pos = 0;//donde voy a comenzar a borrar
                for (int i = 0; i < nombreservicios.length; i++) {
                    idServicios[i] = SVD.obtenerIdServicio(nombreservicios[i]);
                    SVD.modificar(idServicios[i], listaserv_viejo.get(i), Integer.parseInt(turno), placa);//aca va el metodo update
                    if (i == nombreservicios.length - 1) {
                        pos = i;
                    }
                }
                for (int i = pos+1; i < listaserv_viejo.size(); i++) {
                    SVD.borrar(Integer.parseInt(turno), placa, listaserv_viejo.get(i));
                }
                SVD.callProcedureServi();
            }
            if (cantidad < marcadoschecks) {
                //hacer resta y insertar los que faltan con el turno
                int[] idServicios = new int[nombreservicios.length];
                //lista fk servicios viejos
                List<Integer> listaserv_viejo = new LinkedList<>();
                listaserv_viejo = SVD.conFkservicios(Integer.parseInt(turno), placa);
                int pos = 0;//donde voy a comenzar a INSERTAR

                //int ainsertar = nombreservicios.length - listaserv_viejo.size();
                for (int i = 0; i < nombreservicios.length; i++) {
                    idServicios[i] = SVD.obtenerIdServicio(nombreservicios[i]);
                }

                ServiciosPrestados arrayObjetos[] = new ServiciosPrestados[idServicios.length];
                for (int i = 0; i < listaserv_viejo.size(); i++) {

                    SVD.modificar(idServicios[i], listaserv_viejo.get(i), Integer.parseInt(turno), placa);//aca va el metodo update
                    if (i == listaserv_viejo.size() - 1) {
                        pos = i;
                    }
                }
                int fkIdvehiculo = SVD.obtenerfkIdvehiculo(placa);
                for (int i = pos+1; i < idServicios.length; i++) {
                    //amedida que lo obtiene va insertando
                    arrayObjetos[i] = new ServiciosPrestados(Integer.parseInt(turno), idServicios[i], fkIdvehiculo);
//vector de objetos insertando
                    SVD.guardar(arrayObjetos[i]);
                }
                SVD.callProcedureServi();

            }
            
        

                
            

            RequestDispatcher vista = request.getRequestDispatcher("/ModificarServicioPrestado.jsp");
            vista.forward(request, response);

        } catch (Exception ex) {
            Logger.getLogger(EditaServicioPrestado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
