package Controlador;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ReporteServicio", urlPatterns = {"/ReporteServicio"})
public class ReporteServicio extends HttpServlet {


    public static String fecha1 = null;
    public static String fecha2 = null;
    public static String Servicios[] = null;
    public static String tiporeporte = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        Servicios = request.getParameterValues("servicio");
        fecha1 = request.getParameter("fechainicio");
        fecha2 = request.getParameter("fechafin");

        tiporeporte = request.getParameter("tiporeporte");

//         
        RequestDispatcher vista = request.getRequestDispatcher("/serviciosPrestados.jsp");
        vista.forward(request, response);
//
//        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
