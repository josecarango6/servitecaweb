package Controlador;

import Entidades.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.ClienteDAO;
import modelo.DAO.ServicioDAO;

/**
 *
 * @author jooss
 */
@WebServlet(name = "registroCliente", urlPatterns = {"/registroCliente"})
public class registroCliente extends HttpServlet {

    public static int aviso = 0;
    public static int aviso_clienteExiste = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet registroCliente</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet registroCliente at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            String tipoDocumento = request.getParameter("tipoDocumento");
            //consulta id del string
            String numeroD = request.getParameter("numeroDocumento");
            int numeroDocumento=Integer.parseInt(numeroD);
            List<String> listaDocumentos = new LinkedList();
            ServicioDAO documentos = new ServicioDAO();
            listaDocumentos = documentos.obtenerDocumentos();
            if (listaDocumentos.contains(request.getParameter("numeroDocumento"))) {
                aviso_clienteExiste = 1;

                RequestDispatcher vista = request.getRequestDispatcher("/registroCliente.jsp");
                vista.forward(request, response);

            } else {

                String primerNombre = request.getParameter("primerNombre");
                String segundoNombre = request.getParameter("segundoNombre");
                String primerApellido = request.getParameter("primerApellido");
                String segundoApellido = request.getParameter("segundoApellido");
                int telefono = Integer.parseInt(request.getParameter("telefono"));
                String correo = request.getParameter("correo");
                String direccion = request.getParameter("direccion");
                ClienteDAO daocliente = new ClienteDAO();

                int idtipoDoc = daocliente.obtenerIdtipoDccumento(tipoDocumento);
                Cliente u = new Cliente(numeroDocumento, primerNombre, segundoNombre, primerApellido, segundoApellido, telefono, correo, direccion, idtipoDoc);

                daocliente.guardar(u);
                aviso = 1;
                RequestDispatcher vista = request.getRequestDispatcher("/registroCliente.jsp");
                vista.forward(request, response);
            }
        } catch (Exception ex) {
            Logger.getLogger(registroCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
