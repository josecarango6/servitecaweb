package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.DescuentoDAO;
import modelo.DAO.ServicioDAO;

@WebServlet(name = "servicioOpera", urlPatterns = {"/servicioOpera"})
public class servicioOpera extends HttpServlet {

    public static String concatenado = "";
    public static String nombreservicio = "";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

            String operamodifica = request.getParameter("dato");
            String nombreser = request.getParameter("dato2");
            String op = request.getParameter("op");
            if (op.equalsIgnoreCase("modifica")) {

                String[] arrOfStr = operamodifica.split(",");
                nombreservicio = nombreser;
              
                //actualiza
                DescuentoDAO ddao = new DescuentoDAO();
                List<String> lista1 = new LinkedList();
                lista1 = ddao.obtenerCabecercostos();
                //String concatenado = "";
                for (int i = 0; i < arrOfStr.length; i++) {

                    if (i == arrOfStr.length - 1) {
                        concatenado = concatenado + lista1.get(i + 1) + "=" + arrOfStr[i];
                    } else if (i == 0) {
                        concatenado = concatenado + lista1.get(i + 1) + "='" + arrOfStr[i] + "',";
                    } else {
                        concatenado = concatenado + lista1.get(i + 1) + "=" + arrOfStr[i] + ",";
                    }

                }

                RequestDispatcher vista = request.getRequestDispatcher("/servicios.jsp");
                vista.forward(request, response);
            }
 

        } catch (Exception ex) {
            Logger.getLogger(servicioOpera.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
