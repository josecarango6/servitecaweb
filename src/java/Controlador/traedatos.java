/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.ServiciosPrestadosDAO;

/**
 *
 * @author jooss
 */
@WebServlet(name = "traedatos", urlPatterns = {"/traedatos"})
public class traedatos extends HttpServlet {
    
    public static String val3 = "";
    public static int turno;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<script>");
            
            out.println("function prueba1() {");

//enviar por el metodo get tres arreglos con vehiculo,,,costoservicio yyy nombreservicio
            out.println("var  val= window.parent.document.getElementById('turno').value;");
            
            out.println("var  val3= window.parent.document.getElementById('placa').value;");
            
            out.println("window.location.href = 'http://localhost:8080/Serviteca/traedatos?val='+val+'&val3='+val3+'';");
            
            out.println("}");
            out.println("</script>");
            out.println("<title>Servlet tiempo</title>");
            out.println("</head>");
            out.println("<body>");
            
            out.println("<h1>Hello World!</h1>");
            
            out.println("<button id='automatico' onclick='prueba1()'>clic me</button>");

            //prueba1 es la funcion que va dentrodel evento change del checkbox
            String val = request.getParameter("val");//turno

            val3 = request.getParameter("val3");//placa
            if (val!=null) {
                turno = Integer.valueOf(val);
            }
            

            //falta else
            // aqui hago la comsulta a la bd y mando los datos a registrovehiculo.jsp caturandolo como public static
            out.println("</body>");
            out.println("</html>");
            
        } catch (Exception ex) {
            Logger.getLogger(traedatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}
