/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dbutil;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;

public class Mensajes {

    public void enviar(HttpServletResponse response) throws IOException {

        PrintWriter out = response.getWriter();
        out.println("<script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js'></script>");
        out.println("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
        out.println("<script>");
        out.println("$(document).ready(function(){");
        out.println("Swal.fire('Any fool can use a computer')");
        out.println("});");
        out.println("</script>");

    }

    public void enviaryredirecionar(HttpServletResponse response, String text, String resultado, String pagina) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js'></script>");
        out.println("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
        out.println("<script>");
        out.println("$(document).ready(function(){");
        out.println("swal ( 'Sucess' ,  '" + text + "' ,  '" + resultado + "')");
        out.println(".then(function() {");
        out.println("window.location = '" + pagina + "';");
        out.println("});");
        out.println("});");
        out.println("</script>");
    }

    public void mensajealerta(HttpServletResponse response, String pagina, String accion) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js'></script>");
        out.println("<script src='https://cdn.jsdelivr.net/npm/sweetalert2@8'></script>");
        out.println("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
        out.println("<script>");
        out.println("$(document).ready(function(){");
        out.println("let timerInterval");
        out.println("Swal.fire({");
        out.print("showCancelButton: false,");
        out.print("showConfirmButton: false,");
        out.print("showCloseButton: true,");
        out.println("title:'" + accion + "',");
        out.println("timer: 2000000,");
        out.println(" onClose: () => {");
        out.println(" clearInterval(timerInterval)");
        out.println(" }");
        out.println("}).then((result) => {");
        out.println("window.location = '" + pagina + "';");
        out.println("})");
        out.println("})");
        out.println("</script>");
    }

  
}
