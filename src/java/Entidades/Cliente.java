
package Entidades;


public class Cliente  {

  
    private int idCLIENTE;

    private int numeroDocumento;

   
    private String primerNombre;
 
    private String segundoNombre;

    private String primerApellido;

    private String segundoApellido;
 
    private int telefono;
   
    private String correo;
 
    private String direccion;
   
    private int idTIPODOCUMENTO;

    public Cliente() {
    }

    public Cliente(Integer idCLIENTE) {
        this.idCLIENTE = idCLIENTE;
    }



    public Cliente(int numeroDocumento, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, int telefono, String correo, String direccion, int idtipoDoc) {
        this.numeroDocumento = numeroDocumento;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.telefono = telefono;
        this.correo = correo;
        this.direccion = direccion;
        this.idTIPODOCUMENTO = idtipoDoc;
    }



    public int getIdTIPODOCUMENTO() {
        return idTIPODOCUMENTO;
    }

    public void setIdTIPODOCUMENTO(int idTIPODOCUMENTO) {
        this.idTIPODOCUMENTO = idTIPODOCUMENTO;
    }



    public int getIdCLIENTE() {
        return idCLIENTE;
    }

    public void setIdCLIENTE(int idCLIENTE) {
        this.idCLIENTE = idCLIENTE;
    }

    public int getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(int numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }



    
}
