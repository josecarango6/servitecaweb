
package Entidades;

import java.sql.Date;


public class Liquidacion {


    private int FK_idDESCUENTO;
    private Date fechaIngreso;
    private String horaIngreso;
    private int costoServicios;
    private int totalLiquidacion;
    private Date fechaSalida;
    private String horaSalida;
    private String tiempoServicio;
    private int diasServicio;
    private int FK_idVEHICULO;
    private int FK_idESTADO_LIQUIDACION;

    public Liquidacion(int FK_idDESCUENTO, Date fechaIngreso, String horaIngreso, int costoServicios, int totalLiquidacion, Date fechaSalida, String horaSalida, String tiempoServicio, int diasServicio, int FK_idVEHICULO, int FK_idESTADO_LIQUIDACION) {
        this.FK_idDESCUENTO = FK_idDESCUENTO;
        this.fechaIngreso = fechaIngreso;
        this.horaIngreso = horaIngreso;
        this.costoServicios = costoServicios;
        this.totalLiquidacion = totalLiquidacion;
        this.fechaSalida = fechaSalida;
        this.horaSalida = horaSalida;
        this.tiempoServicio = tiempoServicio;
        this.diasServicio = diasServicio;
        this.FK_idVEHICULO = FK_idVEHICULO;
        this.FK_idESTADO_LIQUIDACION = FK_idESTADO_LIQUIDACION;
    }

    public int getDiasServicio() {
        return diasServicio;
    }

    public void setDiasServicio(int diasServicio) {
        this.diasServicio = diasServicio;
    }



    public String getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(String horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

   

    public String getTiempoServicio() {
        return tiempoServicio;
    }

    public void setTiempoServicio(String tiempoServicio) {
        this.tiempoServicio = tiempoServicio;
    }





    public int getFK_idDESCUENTO() {
        return FK_idDESCUENTO;
    }

    public void setFK_idDESCUENTO(int FK_idDESCUENTO) {
        this.FK_idDESCUENTO = FK_idDESCUENTO;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getCostoServicios() {
        return costoServicios;
    }

    public void setCostoServicios(int costoServicios) {
        this.costoServicios = costoServicios;
    }

    public int getTotalLiquidacion() {
        return totalLiquidacion;
    }

    public void setTotalLiquidacion(int totalLiquidacion) {
        this.totalLiquidacion = totalLiquidacion;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public int getFK_idVEHICULO() {
        return FK_idVEHICULO;
    }

    public void setFK_idVEHICULO(int FK_idVEHICULO) {
        this.FK_idVEHICULO = FK_idVEHICULO;
    }

    public int getFK_idESTADO_LIQUIDACION() {
        return FK_idESTADO_LIQUIDACION;
    }

    public void setFK_idESTADO_LIQUIDACION(int FK_idESTADO_LIQUIDACION) {
        this.FK_idESTADO_LIQUIDACION = FK_idESTADO_LIQUIDACION;
    }

}
