package Entidades;

public class ServiciosPrestados {

    private int turno;
 
    private int fKidSERVICIO;
    private int fKidVEHICULO;

    public ServiciosPrestados() {
    }

    public ServiciosPrestados(int turno, int fKidSERVICIO, int fKidVEHICULO) {
        this.turno = turno;

        this.fKidSERVICIO = fKidSERVICIO;
        this.fKidVEHICULO = fKidVEHICULO;
    }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }





    public int getfKidSERVICIO() {
        return fKidSERVICIO;
    }

    public void setfKidSERVICIO(int fKidSERVICIO) {
        this.fKidSERVICIO = fKidSERVICIO;
    }

    public int getfKidVEHICULO() {
        return fKidVEHICULO;
    }

    public void setfKidVEHICULO(int fKidVEHICULO) {
        this.fKidVEHICULO = fKidVEHICULO;
    }

}
