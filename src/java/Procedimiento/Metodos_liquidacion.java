package Procedimiento;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import modelo.DAO.LiquidacionDAO;
import modelo.DAO.ServiciosPrestadosDAO;

public class Metodos_liquidacion {

    public List<String> obtenerNombreServicio(ServiciosPrestadosDAO busca, List<Integer> lstPRESTADOS) throws SQLException {
        String captura = "";
        List<String> lstPRESTADOS2 = new LinkedList();
        for (int i = 0; i < lstPRESTADOS.size(); i++) {

            captura = busca.obtenerNombreServicio(lstPRESTADOS.get(i));
            lstPRESTADOS2.add(captura);

        }
        java.util.Collections.sort(lstPRESTADOS2);//ordena alfabeticamente
        return lstPRESTADOS2;

    }

    public List<Integer> obtenerCostosdeServicioformat(ServiciosPrestadosDAO busca, List<String> lstPRESTADOS2, String tipoveh) throws SQLException {
        List<Integer> listacostosformato = new LinkedList();

        int capturanumFormato = 0;
        for (int i = 0; i < lstPRESTADOS2.size(); i++) {

            capturanumFormato = busca.selectcostoFrmat(tipoveh, lstPRESTADOS2.get(i));
            listacostosformato.add(capturanumFormato);
        }
        return listacostosformato;
    }

    public int sumarcostos(List<Integer> listacostosformato) {
        int sumacostos = 0;
        for (int i = 0; i < listacostosformato.size(); i++) {
            sumacostos = sumacostos + listacostosformato.get(i);
        }
        return sumacostos;
    }

    public int contarservicios(List<Integer> listacostosformato) {
        int contarServicios = 0;
        for (int i = 0; i < listacostosformato.size(); i++) {
            contarServicios = contarServicios + 1;
        }
        return contarServicios;
    }

    public int totalLiquidacion(LiquidacionDAO liqui, int sumacostos, int contarServicios) throws SQLException {
        int cant = liqui.cantidadServicio();
        int porc = 0;
        int totalLiquidacion = 0;
        if (contarServicios >= cant) {
            porc = liqui.PorcentajeDesc();
            totalLiquidacion = (sumacostos * porc) / 100;
            totalLiquidacion = sumacostos - totalLiquidacion;
        }
        if (contarServicios < cant) {
            totalLiquidacion = sumacostos;
        }
        return totalLiquidacion;
    }
        public int totalporcentaje(LiquidacionDAO liqui, int contarServicios) throws SQLException {
        int cant = liqui.cantidadServicio();
        int porc = 0;
  
        if (contarServicios >= cant) {
            porc = liqui.PorcentajeDesc();
 
        }

        return porc;
    }
}
