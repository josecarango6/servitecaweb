
package modelo.DAO;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;


public class AuditoriaDAO {

    private Connection conexion;

    public AuditoriaDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<String> selectAuditoria() throws SQLException {
        List<String> listaAuditoria = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = " select fecha, cambioEfectuado, concat(primerNombre,' ', segundoNombre, ' ',primerApellido, ' ',segundoApellido) as nombre "
                    + " from auditoria inner join usuario on idusuario = fk_idusuario ";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String fecha = rs.getString(1);
                String cambio = rs.getString(2);
                String autor = rs.getString(3);

                listaAuditoria.add(fecha);
                listaAuditoria.add(cambio);
                listaAuditoria.add(autor);

            }
            stmt.close();
        }
        
       
        return listaAuditoria;
    }
}
