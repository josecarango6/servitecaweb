package modelo.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import Dbutil.Database;
import Entidades.Cliente;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClienteDAO {

    private Connection conexion;

    public ClienteDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<Integer> obtenerTodosLosClientes() throws SQLException {
        List<Integer> listaCliente = new LinkedList<>();

        Statement stmt = conexion.createStatement();
        String sql = "SELECT numeroDocumento "
                + " FROM cliente ";
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {

            int numeroDocumento = rs.getInt(1);
            listaCliente.add(numeroDocumento);
        }

        stmt.close();
       
        return listaCliente;
    }

    public void guardar(Cliente u) {
        try {
            String sql = " INSERT INTO cliente ( numeroDocumento, primerNombre, segundoNombre, primerApellido, segundoApellido, telefono, correo, direccion, FK_idTIPO_DOCUMENTO) "
                    + " VALUES(?,?,?,?,?,?,?,?,?)";

            PreparedStatement ps = conexion.prepareStatement(sql);

            ps.setInt(1, u.getNumeroDocumento());
            ps.setString(2, u.getPrimerNombre());
            ps.setString(3, u.getSegundoNombre());
            ps.setString(4, u.getPrimerApellido());
            ps.setString(5, u.getSegundoApellido());
            ps.setInt(6, u.getTelefono());
            ps.setString(7, u.getCorreo());
            ps.setString(8, u.getDireccion());
            ps.setInt(9, u.getIdTIPODOCUMENTO());

            ps.executeUpdate();
            ps.close();
          
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public int obtenerIdtipoDccumento(String tipoDocumento) throws SQLException {

        String sql = " SELECT idTIPO_DOCUMENTO FROM tipo_documento "
                + " WHERE nombreDocumento = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setString(1, tipoDocumento);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        ps1.close();
        
        return idtipoDoc;
    }

    public int ConsultarnumeroDocumento(int cedula) throws SQLException {

        String sql = "SELECT numeroDocumento FROM cliente WHERE numeroDocumento = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps = conexion.prepareStatement(sql);

        // Asigna el idPais al parámetro 1 de la instrucción SQL
        ps.setInt(1, cedula);

        ResultSet rs = ps.executeQuery();

        int cel = 0;
        if (rs.next()) {
            cel = rs.getInt(1);
        }
        ps.close();
        
        return cel;
    }

    public int Consultaridcliente(int cedula) throws SQLException {

        String sql = "SELECT numeroDocumento FROM cliente WHERE idcliente = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps = conexion.prepareStatement(sql);

        // Asigna el idPais al parámetro 1 de la instrucción SQL
        ps.setInt(1, cedula);

        ResultSet rs = ps.executeQuery();

        int cel = 0;
        if (rs.next()) {
            cel = rs.getInt(1);
        }
        ps.close();
        
        return cel;
    }

    public int Consultarfkcliente(int cedula) throws SQLException {

        String sql = "SELECT idcliente FROM cliente WHERE numeroDocumento = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps = conexion.prepareStatement(sql);

        // Asigna el idPais al parámetro 1 de la instrucción SQL
        ps.setInt(1, cedula);

        ResultSet rs = ps.executeQuery();

        int cel = 0;
        if (rs.next()) {
            cel = rs.getInt(1);
        }
        ps.close();
        
        return cel;
    }

}
