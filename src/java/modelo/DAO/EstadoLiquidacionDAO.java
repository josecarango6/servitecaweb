package modelo.DAO;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class EstadoLiquidacionDAO {

    private Connection conexion;

    public EstadoLiquidacionDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<String> obtenerTodosLosEstadosLiquidacion() throws SQLException {
        List<String> listaEstados = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "SELECT  estado from estado_liquidacion";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String nombre = rs.getString(1);

                listaEstados.add(nombre);
            }
            stmt.close();
        }
        
        
        return listaEstados;
    }

    public int obtenerIdestado(String estado) throws SQLException {
        String sql = " SELECT idESTADO_LIQUIDACION from ESTADO_LIQUIDACION "
                + " WHERE ESTADO = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setString(1, estado);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        return idtipoDoc;
    }
        public int obtenerIdDescuento(int porcentaje) throws SQLException {
        String sql = " SELECT idDESCUENTO from Descuento "
                + " WHERE porcentajeDescuento = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, porcentaje);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        ps1.close();
        
        return idtipoDoc;
    }

}
