package modelo.DAO;

import Dbutil.Database;
import Entidades.ServiciosPrestados;
import java.sql.CallableStatement;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiciosPrestadosDAO {

    private Connection conexion;

    public ServiciosPrestadosDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public void guardar(ServiciosPrestados u) {
        try {
            String sql = " INSERT INTO Servicios_Prestados (turno,FK_idSERVICIO,FK_idVEHICULO) "
                    + " VALUES(?,?,?)";

            PreparedStatement ps = conexion.prepareStatement(sql);

            ps.setInt(1, u.getTurno());
            ps.setInt(2, u.getfKidSERVICIO());
            ps.setInt(3, u.getfKidVEHICULO());
            //para cambios

            ps.executeUpdate();
            ps.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(VehiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int obtenerIdServicio(String nombreServicio) throws SQLException {

        String sql = " SELECT idSERVICIO FROM servicio "
                + " WHERE nombreServicio = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setString(1, nombreServicio);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        ps1.close();
        
        return idtipoDoc;
    }

    public String obtenerNombreServicio(int idServicio) throws SQLException {

        String sql = " SELECT nombreServicio FROM servicio  "
                + " WHERE idServicio = ? ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, idServicio);

        ResultSet rs = ps1.executeQuery();

        String idtipoDoc = "";
        if (rs.next()) {
            idtipoDoc = rs.getString(1);
        }
        ps1.close();
       
        return idtipoDoc;
    }

    public int turnoIncrement() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "select turno from servicios_prestados order by idservicios_prestados desc limit 1";
        ResultSet rs = stmt.executeQuery(sql);

        int ultimo = 0;

        if (rs != null && rs.next()) {
            ultimo = rs.getInt(1);
//codigo para tratar al conjunto de registros o al registro obtenido
        } else {
            ultimo = 0;
        }

        stmt.close();
        
       
        return ultimo;
    }

    public int LimiteTurno() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "select limiteTurno from servicios_prestados  ORDER BY idSERVICIOS_PRESTADOS  DESC LIMIT 1;";
        ResultSet rs = stmt.executeQuery(sql);

        int ultimo = 0;

        if (rs != null && rs.next()) {
            ultimo = rs.getInt(1);
//codigo para tratar al conjunto de registros o al registro obtenido
        } else {
            ultimo = -1;
        }

        stmt.close();
       
       
        return ultimo;
    }

    public List<String> selectcostos(String tipo) throws SQLException {
        List<String> listacostos = new LinkedList<>();

        Statement stmt = conexion.createStatement();
        if (tipo != null) {
            String sql = "SELECT costo" + tipo + " "
                    + " FROM servicio order by nombreServicio asc";

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {

                String costo = rs.getString(1);
                listacostos.add(costo);
            }

            stmt.close();
         
        
            return listacostos;
        }
        return null;
    }

    public String selectcostostr(String tipo, String nombreservicio) throws SQLException {

        Statement stmt = conexion.createStatement();
        String sql = "SELECT costo" + tipo + ""
                + " FROM servicio where nombreServicio='" + nombreservicio + "'";
        ResultSet rs = stmt.executeQuery(sql);
        String costo = "";
        while (rs.next()) {

            costo = rs.getString(1);

        }

        stmt.close();
       
        
        return costo;
    }

    public static List<String> getSameVaue(List<String> list1, List<String> list2) {
        List<String> result = new ArrayList<>();
        if (list1.size() > list2.size()) {
            for (String s : list1) {
                if (list2.contains(s)) {
                    result.add(s);
                }
            }
        } else {
            for (String s : list2) {
                if (list1.contains(s)) {
                    result.add(s);
                }
            }
        }
        return result;
    }

    public int selectcostoFrmat(String tipo, String nombreservicio) throws SQLException {

        Statement stmt = conexion.createStatement();
        String sql = "SELECT costo" + tipo + ""
                + " FROM servicio where nombreServicio='" + nombreservicio + "'";
        ResultSet rs = stmt.executeQuery(sql);
        int costo = 0;
        while (rs.next()) {

            costo = rs.getInt(1);

        }

        stmt.close();
        
        
        return costo;
    }

    public List<Integer> conServiSelect(int turno, String placa) throws SQLException {
        List<Integer> listaselect = new LinkedList<>();
        String sql = "select FK_idSERVICIO from servicios_prestados  INNER join "
                + "vehiculo on FK_idVEHICULO=idVEHICULO where estadoTurno='0' and turno = ?  AND placa = ? ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, turno);

        ps1.setString(2, placa);

        ResultSet rs = ps1.executeQuery();

        while (rs.next()) {

            int servicio = rs.getInt(1);
            listaselect.add(servicio);
        }

        ps1.close();
       
     
        return listaselect;
    }

    public String conTipoVehiculoSelect(int turno, String placa) throws SQLException {

        String sql = "select nombreTipoVehiculo from servicios_prestados  INNER join  "
                + " vehiculo on FK_idVEHICULO=idVEHICULO inner join tipo_vehiculo on "
                + " idTIPO_VEHICULO=FK_idTIPO_VEHICULO  where estadoTurno='0' and turno = ?  AND placa = ? limit 1";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, turno);

        ps1.setString(2, placa);

        ResultSet rs = ps1.executeQuery();
        String tipoveh = "";
        while (rs.next()) {

            tipoveh = rs.getString(1);

        }

        ps1.close();
       
      
        return tipoveh;
    }

    public int totalservipresta(int turno, String placa) throws SQLException {

        String sql = "select count(idservicios_prestados) from servicios_prestados "
                + " inner join vehiculo on fk_idvehiculo=idvehiculo  where turno=? and placa= ? ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, turno);
        ps1.setString(2, placa);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        ps1.close();
       
        return idtipoDoc;
    }

    public List<Integer> conFkservicios(int turno, String placa) throws SQLException {
        List<Integer> listaselect = new LinkedList<>();
        String sql = "select fk_idservicio from servicios_prestados "
                + "inner join vehiculo on fk_idvehiculo=idvehiculo where turno = ? and placa=?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, turno);
        ps1.setString(2, placa);

        ResultSet rs = ps1.executeQuery();

        while (rs.next()) {

            int servicio = rs.getInt(1);
            listaselect.add(servicio);
        }

        ps1.close();
       
      
        return listaselect;
    }

    public void modificar(int servicionuevo, int servicioviejo, int turno, String placa) {
        try {
            String sql = " UPDATE Servicios_Prestados AS b "
                    + "INNER JOIN vehiculo AS g ON b.fk_idvehiculo = g.idvehiculo "
                    + "SET b.fk_idservicio= ? "
                    + "WHERE  b.fk_idservicio= ? and b.turno= ? and placa=?";

            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setInt(1, servicionuevo);
            ps.setInt(2, servicioviejo);
            ps.setInt(3, turno);
            ps.setString(4, placa);
            ps.executeUpdate();
            ps.close();
           
       
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void borrar(int turno, String placa, int servicioviejo) {
        try {
            String sql = "DELETE s.* FROM servicios_prestados s "
                    + "INNER JOIN vehiculo n ON s.FK_idVEHICULO = n.idVEHICULO "
                    + "WHERE turno= ? AND placa= ? AND FK_idSERVICIO = ? ";

            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setInt(1, turno);
            ps.setString(2, placa);
            ps.setInt(3, servicioviejo);
            ps.executeUpdate();
            ps.close();
           
      
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int obtenerfkIdvehiculo(String placa) throws SQLException {

        String sql = " SELECT fk_idvehiculo FROM servicios_prestados inner join vehiculo on fk_idvehiculo=idvehiculo WHERE placa = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setString(1, placa);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        ps1.close();
       
        return idtipoDoc;
    }

    public void callProcedureServi() throws SQLException {
        String sql = "{call auditoriaServiPrestados}";
        CallableStatement cs = null;
        cs = conexion.prepareCall(sql);
        cs.executeQuery();
        cs.close();
  
    }

}
