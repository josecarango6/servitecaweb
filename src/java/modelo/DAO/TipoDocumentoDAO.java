package modelo.DAO;

import Dbutil.Database;
import Entidades.TipoDocumento;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class TipoDocumentoDAO {

    private Connection conexion;

    public TipoDocumentoDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<String> obtenerTodosLosTipos() throws SQLException {
        List<String> listaTipos = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "SELECT  nombreDocumento from tipo_documento";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String nombre = rs.getString(1);

                listaTipos.add(nombre);
            }
            stmt.close();
        }
       
        
        return listaTipos;
    }

}
