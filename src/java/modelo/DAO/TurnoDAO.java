package modelo.DAO;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TurnoDAO {

    private Connection conexion;

    public TurnoDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<String> obtenerTurnosactivos() throws SQLException {
        List<String> listaTipos = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "Select turno, vehiculo.placa, group_concat(' ',nombreServicio) as Servicios  from servicios_prestados inner join vehiculo on "
                    + " idVEHICULO=FK_idVEHICULO inner join servicio on idservicio=fk_idservicio where estadoTurno='0'  group by FK_idVEHICULO";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String turno = rs.getString(1);
                String placa = rs.getString(2);
                String servicios = rs.getString(3);

                listaTipos.add(turno);
                listaTipos.add(placa);
                listaTipos.add(servicios);
            }
            stmt.close();
        }
        
        
        return listaTipos;
    }

    public void modificarEstadoturno(int fk_idvehiculo) {
        try {
            String sql = "update servicios_prestados set estadoTurno='1' where FK_idVEHICULO = ?";

            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setInt(1, fk_idvehiculo);
            ps.executeUpdate();
            ps.close();
            
        
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//pendiente de errores faltaria recibir placa en este controlador del siguiente metodo

    public int fkidvehiculo(int turno, String placa) throws SQLException {

        String sql = "SELECT fk_idvehiculo FROM servicios_prestados  "
                + " inner join vehiculo on fk_idvehiculo=idvehiculo  "
                + " WHERE  turno = ? and placa = ?   group by fk_idvehiculo";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, turno);
        ps1.setString(2, placa);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        ps1.close();
        
        return idtipoDoc;
    }

    public int fktipovehiculo(int idvehiculo) throws SQLException {

        String sql = "SELECT fk_idtipo_vehiculo FROM vehiculo "
                + "              WHERE idvehiculo = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, idvehiculo);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        ps1.close();
        
        return idtipoDoc;
    }

    public int obtenerturnosGrande() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "SELECT MAX(turno) AS MAXID FROM  servicios_prestados";
        ResultSet rs = stmt.executeQuery(sql);

        int idMasGrande = 0;

        // cada vez que se ejecuta el método rs.next(), se avanza el cursor una fila 
        // Cuando se alcalza el fin del cursor, la funcion rs.next() retorna false
        if (rs.next()) {
            idMasGrande = rs.getInt("MAXID");
        }

        stmt.close();
        
        
        return idMasGrande;
    }

}
