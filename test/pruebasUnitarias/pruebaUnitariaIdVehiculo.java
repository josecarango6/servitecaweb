package pruebasUnitarias;


import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import modelo.DAO.LiquidacionDAO;

public class pruebaUnitariaIdVehiculo  {
        //LiquidacionDAO liquidacion;

   

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void validarIdVehiculo() {
            try {
                LiquidacionDAO liquidacion=new LiquidacionDAO();
                try {
                    int turno = 19;
                    String placa = "YUH676";
                    //System.out.println("hola" + turno + placa);
                    int id = liquidacion.consultaIdVehiculo(turno, placa);
                    System.out.println(id);
                    if (id==0) {
                        System.out.println("No existe turno o placa");
                        assertEquals(0, id);
                    }
                    if (id!=0) {
                        
                        assertEquals(id, id);
                    }
                    
                } catch (SQLException ex) {
                    Logger.getLogger(pruebaUnitariaIdVehiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            } catch (Exception ex) {
                Logger.getLogger(pruebaUnitariaIdVehiculo.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }
}
