package pruebasUnitarias;

import Procedimiento.Metodos_liquidacion;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.DAO.LiquidacionDAO;
import modelo.DAO.ServiciosPrestadosDAO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class pruebaUnitariaTotalLiquidacion {

    public pruebaUnitariaTotalLiquidacion() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validarLiquidacionConDescuento() {
        try {
            Metodos_liquidacion Pro = new Metodos_liquidacion();
            LiquidacionDAO liqui=new LiquidacionDAO();
      
            try {
               int totalLiquidacion=Pro.totalLiquidacion(liqui, 56000, 3);

              
                System.out.println(totalLiquidacion);
                assertEquals(47600, totalLiquidacion);

            } catch (SQLException ex) {
                Logger.getLogger(pruebaUnitariaIdVehiculo.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(pruebaUnitariaIdVehiculo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
