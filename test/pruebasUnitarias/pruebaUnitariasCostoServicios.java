package pruebasUnitarias;

import Procedimiento.Metodos_liquidacion;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import modelo.DAO.LiquidacionDAO;
import modelo.DAO.ServiciosPrestadosDAO;

public class pruebaUnitariasCostoServicios {
    //LiquidacionDAO liquidacion;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validarCostosServicios() {
        try {
            Metodos_liquidacion Pro = new Metodos_liquidacion();
            ServiciosPrestadosDAO busca = new ServiciosPrestadosDAO();
            List<Integer> lstPRESTADOS = new LinkedList();
            List<Integer> costos = new LinkedList();
            lstPRESTADOS.add(1);

            try {
                List<String> lstPRESTADOS2 = new LinkedList();
                lstPRESTADOS2 = Pro.obtenerNombreServicio(busca, lstPRESTADOS);
                String tipoveh = "Automovil";
                costos = Pro.obtenerCostosdeServicioformat(busca, lstPRESTADOS2, tipoveh);
                System.out.println(costos);
                int precio=costos.get(0);
                assertEquals(8500,precio);

            } catch (SQLException ex) {
                Logger.getLogger(pruebaUnitariaIdVehiculo.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(pruebaUnitariaIdVehiculo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
