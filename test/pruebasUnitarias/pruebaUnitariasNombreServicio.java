package pruebasUnitarias;

import Procedimiento.Metodos_liquidacion;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import modelo.DAO.LiquidacionDAO;
import modelo.DAO.ServiciosPrestadosDAO;

public class pruebaUnitariasNombreServicio {
    //LiquidacionDAO liquidacion;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validarNombreServicios() {
        try {
            Metodos_liquidacion Pro = new Metodos_liquidacion();
            ServiciosPrestadosDAO busca = new ServiciosPrestadosDAO();
            List<Integer> lstPRESTADOS = new LinkedList();
            lstPRESTADOS.add(1);
            lstPRESTADOS.add(2);
            lstPRESTADOS.add(3);
            try {
                List<String> lstPRESTADOS2 = new LinkedList();
                lstPRESTADOS2 = Pro.obtenerNombreServicio(busca, lstPRESTADOS);
                
                String concat="";
                for (int i = 0; i < lstPRESTADOS2.size(); i++) {
                    concat=concat+" "+lstPRESTADOS2.get(i);
                }
                System.out.println(concat);
                assertEquals(" Cambio de Aceite Lavado Polichado", concat);
                
            } catch (SQLException ex) {
                Logger.getLogger(pruebaUnitariaIdVehiculo.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception ex) {
            Logger.getLogger(pruebaUnitariaIdVehiculo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
