<%@page import="modelo.DAO.TurnoDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="modelo.DAO.LiquidacionDAO"%>
<%@page import="static Controlador.traetipo.*"%>
<%@page import="modelo.DAO.ServiciosPrestadosDAO"%> 
<%@page import="Dbutil.Mensajes"%> 
<%@page import="modelo.DAO.ClienteDAO"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="java.util.List"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="modelo.DAO.TipoVehiculoDAO"%> 
<!doctype html> 
<html lang="en"> 
    <%
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
        String placa = request.getParameter("placa");
        int turno = Integer.parseInt(request.getParameter("turno"));


    %> 
    <head> 
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <meta name="description" content=""> 
        <meta name="author" content=""> 
        <title>Editar serv.Prestados</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css"> 
        <!-- Font Awesome -->         
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"> 
        <!-- Bootstrap core CSS -->         
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"> 
        <!-- Material Design Bootstrap -->         
       <!--  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.11/css/mdb.min.css" rel="stylesheet">  --> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>         
        <script src="select2/select2-4.0.2/dist/js/select2.min.js" type="text/javascript"></script>
        <!--clockpicker-->         
        <script src="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>         
        <link rel="stylesheet" href="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css"> 
        <!--clockpicker-->         
    </head>     
    <body onload="consultaremio();"> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item">
                    <a href="index.jsp">Inicio</a>
                </li>                 
                <li class="breadcrumb-item active text-white">Registro Vehiculo </li>
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <%@include file="menu.jsp" %> 
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">EDITAR sERVICIOS pRESTADOS</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <div class="row text-left"> 
                        <div class="order-md-1 text-left pr-5 pb-5 pl-5 col-md-12"> 
                            <div class="row"> 
                                <h4 class="col-md-4 mr-auto ml-auto mt-3 mb-3">Datos del Vehiculo</h4> 
                                <h4 class="col-md-4 mr-auto ml-auto mt-3 mb-3">Servicios Requeridos</h4> 
                                <h4 class="col-md-4 mr-auto ml-auto mt-3 mb-3">Costo del Servicio</h4> 
                            </div>                             
                            <form name = "form" onsubmit = "validate(event, this);" class="container" id="needs-validation" novalidate novalidate="" method="post" action="EditaServicioPrestado"> 
                                <div class="row"> 
                                    <div class="mb-3 col-md-2 col-lg-3">Tipo de Vehiculo
                                        <script>


                                            document.addEventListener("DOMContentLoaded", function (event) {

                                            select();
                                            if (document.readyState !== "complete") {
                                            localStorage.clear();
                                            }





                                            });
                                            function select() {
                                            var select = document.getElementById("select");
                                            for (var i = 0; i < tipos.length; i++) {
                                            consulta(i);
                                            }
                                            var peticion = localStorage.getItem("pos");
                                            select.selectedIndex = peticion;
                                            consulta(peticion - 1);
                                            darformato();
                                            }
                                            function traeservlet() {

                                            window.frames['ventana_iframe'].document.getElementById('automatico').click();
                                            location.reload();
                                            }

                                        </script>
                                        <br> 
                                        <%                                            List<String> lstTipoVehiculo = new LinkedList();
                                            TipoVehiculoDAO tipo = new TipoVehiculoDAO();
                                            lstTipoVehiculo = tipo.obtenerTodosLosVehiculos();%> 
                                        <select id="select" onchange="for (var i = 0; i < tipos.length; i++) {
                                            consulta(i);
                                            }
                                            ;
                                            darformato();
                                            traeservlet()" class="custom-select d-block w-100" required="" id="tipoVehiculo" name="tipoVehiculo"> 
                                            <option value="" disabled selected="disabled selected">seleccione una opcion</option>                                             
                                            <%
                                                //LLenandose :)
                                                for (int i = 0; i < lstTipoVehiculo.size(); i++) {%> 
                                                <option name="tipos"> 
                                                    <%out.print(lstTipoVehiculo.get(i));%> 
                                                </option>                                                 
                                            <%  }%> 
                                        </select>                                         
                                        <iframe style="display: none" id='frm1' src='traetipo' marginwidth='0' marginheight='0' name='ventana_iframe' scrolling='no' border='0' frameborder='0' width='300' height='200'>
</iframe>                                         
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
</div>                                         
                                    </div>
                                    <div class="mb-3 col-md-1">
</div>                                     
                                    <!---->
                                    <!-- -->                                     
                                    <div class="btn-group-vertical col-md-6 col-lg-4" data-toggle="buttons"> 
                                        <%
                                            //prueba
                                            List<String> lstDoc = new LinkedList();
                                            ServicioDAO tiposervicio = new ServicioDAO();
                                            lstDoc = tiposervicio.obtenerTodosLosServicios();//LLenandose :)
                                            for (int i = 0; i < lstDoc.size(); i++) {%> 
                                            <label  name="labels" class="btn btn-secondary" for="<%out.print(lstDoc.get(i));%>"> 
                                                <input type="checkbox" onchange="noseve()" value="<%out.print(lstDoc.get(i));%>" autocomplete="off" id="<%out.print(lstDoc.get(i));%>" name="servicio"> 
                                                <%out.print(lstDoc.get(i));
                                            %> 
                                            </label >                                             
                                        <%}%> 
                                    </div>                                     
                                    <div class="col-md-6 col-lg-4"> 
                                        <%for (int i = 0; i < lstDoc.size(); i++) {%> 
                                            <input readonly="" class="form-control mb-2 " name='costos' id="costos<%out.print(lstDoc.get(i));%>" placeholder="" value=""> 
                                            <div class="invalid-feedback"> 
</div>                                             
                                        <%}%> 
                                        <script>
                                            function darformato() {//le da formato decimal a los precios
                                            var valores = document.getElementsByName("costos");
                                            for (var i = 0; i < valores.length; i++) {
                                            if (valores[i].value !== "") {
                                            valores[i].value = valores[i].value
                                                    .replace(/\D/g, "")
                                                    .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                                            }
                                            }
                                            }

                                        </script>                                         
                                        <%

                                            ServiciosPrestadosDAO servicostos = new ServiciosPrestadosDAO();
                                            List<String> lstcostos1 = new LinkedList();

                                            if (vehiculocosto != null) {
                                                lstcostos1 = servicostos.selectcostos(vehiculocosto);
                                            }
                                            //Agregando por cadena

                                        %> 
                                    </div>                                     
                                    <input class="btn btn-primary btn-lg btn-block mb-5 m-auto col-md-3" onclick="$('#select').change();" type="button" value="Ver Costos">
                                </div>                                 
                                <div class="row">
                                    <script>

                                        function tiposelect() {
                                        var option = document.getElementById("tiporeporte");
                                        var checks = document.getElementById('buttons1');
                                        if (option.value === "2") {

                                        checks.style.visibility = 'visible';
                                        } else {
                                        checks.style.visibility = 'hidden';
                                        }


                                        }

                                    </script>
                                    <div id="buttons1" class="mb-3 col-md-3 mt-2 ml-auto mr-auto col-lg-4" style="visibility: hidden">Documento
                                        <div class="container-fluid"> 
                                            <div class="row"> 
                                                <select class="SelectDocumento" name="documento" style="height: 38px; width: 100%;"> 
</select>                                                 
                                            </div>                                             
                                        </div>                                         
                                        <script>
                                            var data = [];
                                        <% List<String> listaDocumentos = new LinkedList();
                                            ServicioDAO documentos = new ServicioDAO();
                                            listaDocumentos = documentos.obtenerDocumentos();

                                            for (int i = 0; i < listaDocumentos.size(); i++) {%>
                                            data[<%out.print(i);%>] = "<%out.print(listaDocumentos.get(i));%>";
                                        <%}%>

                                            var placeholder = "select";
                                            $(".SelectDocumento").select2({
                                            data: data,
                                                    placeholder: placeholder,
                                                    allowClear: false,
                                                    minimumResultsForSearch: 1
                                            });
                                    </script>                                         
                                        <br> 
                                    </div>
                                    <div class="col-md-6 mb-3 col-lg-4"> 
                                        <h4 class="m-auto text-center">Costo Total Servicios</h4> 
                                        <input type="text" class="form-control" id="costoServicios" placeholder="" style="display: none"> 
                                        <input type="button" class="form-control" id="costoServicios2" onclick="this.value = resultado;" placeholder="" value="calcular"> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                    <!--<div class="invalid-feedback"> 
                                            Valid first name is required.
                                        </div>  -->
                                </div>                                 
                                <div class="row"> 
                                    <div class="mb-3 col-md-3">Placa del Vehiculo
                                        <br> 
                                        <input type="text" class="form-control" name="placa" readonly="" id="placa" placeholder="" value="<%if (placa != null) {
                                                out.print(placa);
                                            }
                                               %>" required=""> 
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
</div>                                         
                                    </div>                                     
                                    <div class="mb-3 col-md-3">Turno
                                        <br> 
                                        <input type="text" class="form-control" name="turno" readonly="" id="turno" placeholder="" value="<%if (turno != 0) {
                                                out.print(turno);
                                            }
                                               %>" required=""> 
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
</div>                                         
                                    </div>                                     
                                    <!--BOTON DE CONSULTA-->                                     
                                    <input class="btn btn-primary btn-lg btn-block mb-5 m-auto col-md-3" style="display: none" onclick="window.frames['ventana_iframe'].document.getElementById('automatico').click();
                                        location.reload();" type="button" value="Consultar" id="consultare"> 
                                    <iframe style="display: none" d='frm1' src='traedatos' marginwidth='0' marginheight='0' name='ventana_iframe' scrolling='no' border='0' frameborder='0' width='300' height='200'> 
</iframe>                                     
                                    <!--este consulta-->                                     
                                    <div class="invalid-feedback"> 
                                        Valid last name is required.
</div>                                     
                                </div>                                 
                                <!-- <hr class="bg-primary"> 
                                <div class="row p-auto mr-auto ml-auto mb-2 mt-2"> 
                                    <h4 class="m-5 m-auto col-md-4">Servicios Realizados</h4> 
                                    <h4 class="m-auto col-md-4">Costo del Servicio</h4> 
                                    <h4 class="m-auto col-md-4">Costo Total Servicioso</h4> 
                                </div>  -->                                 
                                <!-- -->
                                <div class="row" style="display: none"> 
                                    <div class="btn-group-vertical col-md-4 " data-toggle="buttons"> 
                                        <%//trae del servlet traedatos

                                            //prueba
                                            List<Integer> lstPRESTADOS = new LinkedList();
                                            List<String> lstPRESTADOS2 = new LinkedList();

                                            ServiciosPrestadosDAO buscaid = new ServiciosPrestadosDAO();
                                            ServiciosPrestadosDAO busca = new ServiciosPrestadosDAO();
                                            //tipo vehiculo

                                            String tipoveh = "";
                                            LiquidacionDAO ConsultaID = new LiquidacionDAO();
                                            int idvehiculo = ConsultaID.consultaIdVehiculo(turno, placa);

                                            //id vehiculo
                                            tipoveh = busca.conTipoVehiculoSelect(turno, placa);
                                            lstPRESTADOS = buscaid.conServiSelect(turno, placa);
                                            //setiar
                                            //turno = 0;

                                            //placa= "";
                                            String captura = "";

                                            for (int i = 0; i < lstPRESTADOS.size(); i++) {

                                                captura = busca.obtenerNombreServicio(lstPRESTADOS.get(i));
                                                lstPRESTADOS2.add(captura);

                                            }
                                            java.util.Collections.sort(lstPRESTADOS2);//ordena alfabeticamente
                                            for (int i = 0; i < lstPRESTADOS2.size(); i++) {%> 
                                            <label class="btn btn-secondary" for="<%out.print(lstPRESTADOS2.get(i));%>"> 
                                                <input type="checkbox" checked="" disabled="disabled" value="<%out.print(lstPRESTADOS2.get(i));%>" autocomplete="off" d="<%out.print(lstPRESTADOS2.get(i));%>" name="servicios"> 
                                                <%out.print(lstPRESTADOS2.get(i));
                                            %> 
                                            </label>                                             
                                        <%}%> 
                                    </div>                                     
                                    <div class="col-md-6 col-lg-4"> 
                                        <%

                                            List<String> listacostos = new LinkedList();
                                            List<Integer> listacostosformato = new LinkedList();
                                            String captura2 = "";
                                            int capturanumFormato = 0;
                                            for (int i = 0; i < lstPRESTADOS2.size(); i++) {
                                                captura2 = busca.selectcostostr(tipoveh, lstPRESTADOS2.get(i));
                                                listacostos.add(captura2);
                                                capturanumFormato = busca.selectcostoFrmat(tipoveh, lstPRESTADOS2.get(i));
                                                listacostosformato.add(capturanumFormato);

                                            }%> 
                                        <% NumberFormat nf = NumberFormat.getInstance();// le da formato al numero
                                            nf = NumberFormat.getIntegerInstance();

                                            int sumacostos = 0;
                                            int contarServicios = 0;
                                            for (int i = 0; i < listacostos.size(); i++) {%> 
                                            <button class="form-control mb-2" id="<%out.print(listacostos.get(i));%>" disabled="disabled"> 
                                                <%out.print(nf.format(listacostosformato.get(i)));%> 
                                                <%sumacostos = sumacostos + Integer.parseInt(listacostos.get(i));%> 
                                                <%contarServicios = contarServicios + 1;%> 
                                            </button>                                             
                                        <%}

                                        %> 
                                        <div class="invalid-feedback"> 
</div>                                         
                                    </div>                                     
                                    <div class="col-md-4 mb-0"> 
                                        <input type="text" class="form-control mb-5" id="costoServicios" name="costoServicios" placeholder="" value="<%out.print(sumacostos);%>" required="" readonly="readonly"> 
                                        <div class="invalid-feedback"> 
                                            Valid last name is required.
</div>                                         
                                    </div>                                     
                                </div>                                 
                                <script>
                                    function consultaremio(){

                                        <%

                                            List<String> result = new ArrayList();
                                            result = busca.getSameVaue(lstPRESTADOS2, lstDoc);
                                            for (int i = 0; i < result.size(); i++) {%>

                                   
                                    document.getElementById('<%out.print(result.get(i));%>').checked = true;
                                   
                                                <%}%>
       <%
           TurnoDAO turnito = new TurnoDAO();
           int fkidvvehi = turnito.fkidvehiculo(turno, placa);
           int tipovehis = turnito.fktipovehiculo(
                   fkidvvehi);%>
                                    var indes = 0;
                                    var indes =<%out.print(tipovehis);%>;
                                    document.getElementById("select").selectedIndex = indes;
                                    }





                                       </script>
                                <!-- -->
                                <script>
                                    function validate(e) {
                                    var formulario = document.form;
                                    var al_menos_uno = false;
                                    for (var i = 0; i < formulario.servicio.length; i++) {
                                    if (formulario.servicio[i].checked) {
                                    al_menos_uno = true;
                                    }
                                    }

                                    if (!al_menos_uno){
                                    alert ('debes seleccionar al menos una opci�n');
                                    if (e.preventDefault) {
                                    e.preventDefault();
                                    } else {
                                    e.returnValue = false;
                                    }
                                    }
                                    }
                                </script>
                                <script>
                                    //obtener arreglo de costos y arreglo de servicios y arreglo de tipos
                                    var costos = document.getElementsByName("costos");
                                    var servicios = document.getElementsByName("servicio");
                                    var tipos = document.getElementsByName("tipos");
                                    var fd3 = document.getElementsByName("costoServicios2");
                                    var val = [];
                                    var resultado = 0;
                                    function noseve() {

                                    for (var i = 0; i < servicios.length; i++) {
                                    if (!servicios[i].checked) {
                                    costos[i].style.backgroundColor = 'gray';
                                    }

                                    if (servicios[i].checked) {
                                    costos[i].style.backgroundColor = 'white';
                                    }

                                    }
                                    resultado = 0;
                                    for (var i = 0; i < costos.length; i++) {
                                    if (costos[i].style.backgroundColor === 'white') {
                                    var str = costos[i].value;
                                    var array = str.split(".");
                                    var acum = "";
                                    for (var j = 0; j < array.length; j++) {
                                    acum = acum + array[j];
                                    }


                                    resultado = resultado + parseInt(acum);
                                    }

                                    }
                                    resultado = resultado.toString().replace(/\D/g, "")
                                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                                    }

                                    function recorre(w) {
                                    if (tipochange() === w) {
                                    <%for (int i = 0; i < lstcostos1.size(); i++) {%>
                                    val[w] = document.getElementById("" + costos[<%out.print(i);%>].getAttribute('id') + "").value = <%out.print(lstcostos1.get(i));%>;
                                    <%}%>

                                    }



                                    }

                                    function consulta(p) {
                                    var pos = tipochange(tipos);
                                    for (var i = 0; i < tipos.length; i++) {


                                    if (servicios[p].checked) {//consulta
                                    if (pos === i) {
                                    recorre(i);
                                    localStorage.setItem("pos", pos + 1);
                                    break;
                                    }


                                    //alert("tipo de vehiculo " + pos + " servicio " + servicios[i].value + " se inserta en " + costos[i].getAttribute("id"));
                                    } else if (!servicios[p].checked) {
                                    if (pos === i) {
                                    recorre(i);
                                    localStorage.setItem("pos", pos + 1);
                                    break;
                                    }
                                    }

                                    }


                                    }
                                    function tipochange(tiposdevehiculos) {
                                    var opt;
                                    for (var i = 0, len = tipos.length; i < len; i++) {
                                    opt = tipos[i];
                                    if (opt.selected === true) {
                                    break;
                                    }
                                    }
                                    return i;
                                    }
                                </script>                                 
                                <hr class="mb-4"> 
                                <button class="btn btn-lg btn-block mb-5 ml-auto mr-auto col-md-5 col-lg-4 btn-primary" type="submit">Editar Servicios</button>                                 
                            </form>                             
                            <script src="assets/js/jquery.min.js" script>
                                        <script type="bootstrap/js/bootstrap.min.js" </script>
                                        <script>
                                        // Example starter JavaScript for disabling form submissions if there are invalid fields
                                        (function() {
                                            'use strict';
                                    window.addEventListener('load', function() {
                                    var form = document.getElementById('needs-validation');
                                    form.addEventListener('submit', function(event) {
                                    if (form.checkValidity() === false) {
                                    event.preventDefault();
                                    event.stopPropagation();
                            }
                                    form.classList.add('was-validated');
                                        }, false);
                                            }, false);
                                            })();
</script>                             
                        </div>                         
                    </div>                     
                </main>                 
            </div>             
        </div>         
        <!-- Bootstrap core JavaScript
        ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->         
        <script src="assets/js/jquery.min.js"></script>         
        <script src="assets/js/popper.js"></script>         
        <script src="bootstrap/js/bootstrap.min.js"></script>         
        <!-- Icons -->         
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>         
                                            <script>feather.replace()</script>         
        <!-- Graphs -->         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>         
                                            <script>
                                                var ctx = document.getElementById("myChart");
                                                var myChart = new Chart(ctx, {
                                    type: 'line',
                                            data: {
                                            labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                    datasets: [{
                                                    data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                            lineTension: 0,
                                                            backgroundColor: 'transparent',
                                                            borderColor: '#007bff',
                                                            borderWidth: 4,
                                                            pointBackgroundColor: '#007bff'
                                        }]
                                        },
                                            options: {
                                                            scales: {
                                                            yAxes: [{
                                                            ticks: {
                                                            beginAtZero: false
                                            }
                                            }]
                                            },
                                                    legend: {
                                                                    display: false,
                                            }
                                            }
                                            });
</script>         
    </body>     
</html>
