<%-- 
    Document   : registroCliente
    Created on : 8/09/2019, 07:11:12 PM
    Author     : jooss
--%> 
<%@page import="modelo.DAO.DescuentoDAO"%> 
<%@page import="modelo.DAO.TipoDocumentoDAO"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="Entidades.TipoDocumento"%> 
<%@page import="java.util.List"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@page import=" static Controlador.consultaCliente.*"%> 
<!doctype html> 
<html> 
    <%
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
%> 
    <head> 
        <title>Registro Cliente</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <link href="bootstrap/css/searchbox.css" rel="stylesheet" type="text/css"/> 
    </head>     
    <body> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item"> 
                    <a href="index.jsp">Inicio</a> 
                </li>                 
                <li class="breadcrumb-item active text-white">Descuentos</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <%@include file="menu.jsp" %> 
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mb-5"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Auditoria</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <!--cuadro de busqueda-->                                 
                                <div class='search-container'> 
                                    <script>
                                        function guarda() {
                                            var val = document.getElementById('busqueda').value;
                                            return val;
                                        }
                                        function stopSubmit(e) {
                                            var inDay = document.getElementById('busqueda').value;

                                            if (inDay == "") {
                                                return false;
                                            }
                                        }
                                    </script>                                     
                                    <a id="accionar" style="display: none" href="consultaCliente" onclick="stopSubmit();
                                            window.location.reload(true);
                                            window.open(this.href += '?cedula=' + guarda(), 'newwindow', 'width=300, height=250, top=' + (screen.height - 250) / 2 + ',left=' + (screen.width - 300) / 2 + ',location=no');
                                            return false;" target="_blank" class='button'> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 24 24"> 
                                            <path d="M15.5 12a4.5 4.5 0 0 1 3.807 6.9l3.084 3.084-1.407 1.407-3.108-3.069A4.5 4.5 0 1 1 15.5 12zm0 2a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM10 4a4 4 0 0 1 3.18 6.426 6.505 6.505 0 0 0-2.268 1.47L10 12a4 4 0 0 1 0-8zM2 20v-2c0-2.124 3.312-3.862 7.495-3.992A6.48 6.48 0 0 0 9 16.5a6.47 6.47 0 0 0 1.022 3.5H2z" fill="#626262"/> 
                                        </svg> </a> 
                                </div>                                 
                                <!--cuadro de busqueda-->                                 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <div class="row text-left"> 
                        <div class="order-md-1 text-left bg-white pr-5 pb-5 pl-5 col-md-12">
                            <h4 class="mb-3 mt-auto">Visualización de cambios realizado</h4>
                            <table class="table"> 
                                <thead> 
                                    <tr> 
                                        <th>Fecha</th> 
                                        <th>Cambio Realizado</th> 
                                        <th>Autor del Cambio</th> 
                                    </tr>                                     
                                </thead>                                 
                                <tbody> 
                                    <tr> 
                                        <th scope="row">12/11/2019</th> 
                                        <td>porcentaje 35 cantidad 3</td> 
                                        <td>Juan Carlos Gomez Zapata</td> 
                                    </tr>                                     
                                    <tr> 
                                        <th scope="row">12/11/2019</th> 
                                        <td>porcentaje 35 cantidad 3</td> 
                                        <td>Juan Carlos Gomez Zapata</td> 
                                    </tr>                                     
                                    <tr> 
                                        <th scope="row">12/11/2019</th> 
                                        <td>porcentaje 35 cantidad 3</td> 
                                        <td>Juan Carlos Gomez Zapata</td> 
                                    </tr>                                     
                                </tbody>                                 
                            </table>                             
                        </div>                         
                        <hr class="mb-4">
                    </form>                     
            </div>             
        </div>         
    </main>     
</div> 
<!-- Bootstrap core JavaScript
                ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="assets/js/jquery.min.js"></script> 
<script src="assets/js/popper.js"></script> 
<script src="bootstrap/js/bootstrap.min.js"></script> 
<!-- Icons --> 
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script> 
<script>feather.replace()</script> 
<!-- Graphs --> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script> 
<script>
                                        var ctx = document.getElementById("myChart");
                                        var myChart = new Chart(ctx, {
                                            type: 'line',
                                            data: {
                                                labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                datasets: [{
                                                        data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                        lineTension: 0,
                                                        backgroundColor: 'transparent',
                                                        borderColor: '#007bff',
                                                        borderWidth: 4,
                                                        pointBackgroundColor: '#007bff'
                                                    }]
                                            },
                                            options: {
                                                scales: {
                                                    yAxes: [{
                                                            ticks: {
                                                                beginAtZero: false
                                                            }
                                                        }]
                                                },
                                                legend: {
                                                    display: false,
                                                }
                                            }
                                        }
                                        );
            </script>
