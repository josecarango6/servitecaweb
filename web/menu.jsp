<%@page import="java.util.List"%>
<%@page import="java.util.LinkedList"%>
<%@page import="modelo.DAO.UsuarioDAO"%>
<ul class="nav flex-column"> 
    <li class="nav-item"> 
        <a style="display: none"class="nav-link active" href="index.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"> 
                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>                                         
                <polyline points="9 22 9 12 15 12 15 22"></polyline>                                         
            </svg> 
            Inicio</a> 
    </li>                             
    <li class="nav-item"> 
        <a  style="display: none"class="nav-link" href="liquidacion.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"> 
                <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>                                         
                <polyline points="13 2 13 9 20 9"></polyline>                                         
            </svg> 
            Liquidaci�n</a> 
    </li>                             
    <li class="nav-item"> 
        <a style="display: none" class="nav-link" name="registroVehiculo" href="registroVehiculo.jsp" target=""><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 11 11"> 
                <path d="M9 4l-.89-2.66A.5.5 0 0 0 7.64 1H3.36a.5.5 0 0 0-.47.34L2 4a1 1 0 0 0-1 1v3h1v1a1 1 0 1 0 2 0V8h3v1a1 1 0 1 0 2 0V8h1V5a1 1 0 0 0-1-1zM3 7a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm0-3l.62-2h3.76L8 4H3zm5 3a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" fill="#626262"></path>                                         
            </svg> 
            Registro Veh�culo</a> 
    </li>                             
    <li class="nav-item"> 
        <a style="display: none"class="nav-link" href="registroCliente.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"> 
                <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>                                         
                <circle cx="9" cy="7" r="4"></circle>                                         
                <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>                                         
                <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>                                         
            </svg>                                Registro Cliente</a> 
    </li>                             
    <li class="nav-item"> 
        <a style="display: none"class="nav-link" href="servicios.jsp"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 20 20"> 
                <path d="M4 5H.78c-.37 0-.74.32-.69.84l1.56 9.99S3.5 8.47 3.86 6.7c.11-.53.61-.7.98-.7H10s-.7-2.08-.77-2.31C9.11 3.25 8.89 3 8.45 3H5.14c-.36 0-.7.23-.8.64C4.25 4.04 4 5 4 5zm4.88 0h-4s.42-1 .87-1h2.13c.48 0 1 1 1 1zM2.67 16.25c-.31.47-.76.75-1.26.75h15.73c.54 0 .92-.31 1.03-.83.44-2.19 1.68-8.44 1.68-8.44.07-.5-.3-.73-.62-.73H16V5.53c0-.16-.26-.53-.66-.53h-3.76c-.52 0-.87.58-.87.58L10 7H5.59c-.32 0-.63.19-.69.5 0 0-1.59 6.7-1.72 7.33-.07.37-.22.99-.51 1.42zM15.38 7H11s.58-1 1.13-1h2.29c.71 0 .96 1 .96 1z" fill="#626262"></path>                                         
            </svg> 
            Servicios</a> 
    </li>                             
    <li class="nav-item"> 
        <a style="display: none"class="nav-link" href="descuento.jsp"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 1024 1024"> 
                <path d="M855.7 210.8l-42.4-42.4a8.03 8.03 0 0 0-11.3 0L168.3 801.9a8.03 8.03 0 0 0 0 11.3l42.4 42.4c3.1 3.1 8.2 3.1 11.3 0L855.6 222c3.2-3 3.2-8.1.1-11.2zM304 448c79.4 0 144-64.6 144-144s-64.6-144-144-144-144 64.6-144 144 64.6 144 144 144zm0-216c39.7 0 72 32.3 72 72s-32.3 72-72 72-72-32.3-72-72 32.3-72 72-72zm416 344c-79.4 0-144 64.6-144 144s64.6 144 144 144 144-64.6 144-144-64.6-144-144-144zm0 216c-39.7 0-72-32.3-72-72s32.3-72 72-72 72 32.3 72 72-32.3 72-72 72z" fill="#626262"></path>                                         
            </svg> 
            Descuentos</a>
        <a style="display: none"class="nav-link" href="turno.jsp"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 24 24">
                <path opacity=".3" d="M5 19h14V5H5v14zm2.41-7.41L10 14.17l6.59-6.59L18 9l-8 8l-4-4l1.41-1.41z" fill="#626262"/>
                <path d="M18 9l-1.41-1.42L10 14.17l-2.59-2.58L6 13l4 4zm1-6h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04a2.008 2.008 0 0 0-1.44 1.19c-.1.24-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55c.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75s-.75-.34-.75-.75s.34-.75.75-.75zM19 19H5V5h14v14z" fill="#626262"/>
            </svg> 
            Turno</a> 
    </li>                             
</ul>                         
<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted"> <span  class="text-primary">Reportes</span> <a class="d-flex align-items-center text-muted" href="#"> </a> </h6> 
<ul class="nav flex-column mb-2"> 
    <li class="nav-item"> 
        <a style="display: none"class="nav-link" href="serviciosPrestados.jsp"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"> 
                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>                                         
                <polyline points="14 2 14 8 20 8"></polyline>                                         
                <line x1="16" y1="13" x2="8" y2="13"></line>                                         
                <line x1="16" y1="17" x2="8" y2="17"></line>                                         
                <polyline points="10 9 9 9 8 9"></polyline>                                         
            </svg> 
            Cantidad de Servicios Prestados y Tiempo de Atenci�n</a> 
    </li>                             
    <li class="nav-item"> 
        <a style="display: none"class="nav-link" href="serviciosPrestadosCV.jsp"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"> 
                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>                                         
                <polyline points="14 2 14 8 20 8"></polyline>                                         
                <line x1="16" y1="13" x2="8" y2="13"></line>                                         
                <line x1="16" y1="17" x2="8" y2="17"></line>                                         
                <polyline points="10 9 9 9 8 9"></polyline>                                         
            </svg>Servicios Prestados por Cliente o Automotor</a> 
    </li>   
    
    <li class="nav-item"> 
        <a style="display: none"class="nav-link" href="auditoria.jsp"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><path d="M0 168v-16c0-13.255 10.745-24 24-24h360V80c0-21.367 25.899-32.042 40.971-16.971l80 80c9.372 9.373 9.372 24.569 0 33.941l-80 80C409.956 271.982 384 261.456 384 240v-48H24c-13.255 0-24-10.745-24-24zm488 152H128v-48c0-21.314-25.862-32.08-40.971-16.971l-80 80c-9.372 9.373-9.372 24.569 0 33.941l80 80C102.057 463.997 128 453.437 128 432v-48h360c13.255 0 24-10.745 24-24v-16c0-13.255-10.745-24-24-24z" fill="#626262"/></svg> 
                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>                                         
                <polyline points="14 2 14 8 20 8"></polyline>                                         
                <line x1="16" y1="13" x2="8" y2="13"></line>                                         
                <line x1="16" y1="17" x2="8" y2="17"></line>                                         
                <polyline points="10 9 9 9 8 9"></polyline>                                         
            </svg>Auditoria</a> 
    </li> 
    <li class="nav-item"> 
    </li>                             
    <li class="nav-item"> 
    </li>                             
</ul> 
<%
    String tipoUsuariomenu = (String) session.getAttribute("tipoUsuario");
    List<String> listausuario = new LinkedList();
    List<String> listaurls = new LinkedList();
    if (tipoUsuariomenu == "2") {
        UsuarioDAO us = new UsuarioDAO();

        listausuario = us.Permisodeusuario(Integer.parseInt(tipoUsuariomenu));
        listaurls = us.Permisodeurls(Integer.parseInt(tipoUsuariomenu));
%>
<script>
    var reporte = document.getElementsByTagName("span");
    reporte[0].style.display = "none";
    var pagina = location.pathname.substring(11);
    //var nombrepagina=pagina.split(".");
    //pagina=nombrepagina[0];
    //alert(pagina);estoy capturando el nombre de l apagina para comparar con la lista permiso y denegar la entrada por url
    var menulista = document.getElementsByClassName("nav-link");




///////////////////////////////////////////////////////////////
    for (var i = 0; i < menulista.length; i++) {
        var str = menulista[i].innerText;
        // alert(str.trim());
    <%for (int j = 0; j < listausuario.size(); j++) {

    %>
        if (str.trim() === '<%out.print(listausuario.get(j));%>') {
            menulista[i].style.display = "block";
        }

    <%}%>

    }
    //alert(menulista[1].innerText);
</script><%
    }
    if (tipoUsuariomenu == "1") {%>
<script>

    var menulista = document.getElementsByClassName("nav-link");
    for (var i = 0; i < menulista.length; i++) {

        menulista[i].style.display = "block";
    }
</script>
<%}

%>
<script>
    var lista1 = [];
    <%for (int j = 0; j < listaurls.size(); j++) {%>
           lista1[<%out.print(j);%>] ='<%out.print(listaurls.get(j));%>';
           
      <%  }%>
    
 

    if (!lista1.includes(pagina)) {
        alert("no tiene permiso");

        window.location.href = "http://localhost:8080/Serviteca/index.jsp";




    }
</script>

