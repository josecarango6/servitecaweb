<%-- 
    Document   : index
    Created on : 8/09/2019, 4:11:44 p. m.
    Author     : jooss
--%> 
<%@page import="modelo.DAO.ClienteDAO"%> 
<%@page import="modelo.DAO.ReportesDAO"%> 
<%@page import="java.text.DateFormat"%> 
<%@page import="java.sql.Date"%> 
<%@page import="java.text.SimpleDateFormat"%> 
<%@page import="modelo.DAO.ServiciosPrestadosDAO"%> 
<%@page import="java.util.List"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="static Controlador.ReporteServicioCV.*"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!doctype html> 
<html lang="en"> 
    <head> 
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <meta name="description" content=""> 
        <meta name="author" content=""> 
        <title>Dashboard Template for Bootstrap</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css"> 
        <!-- Font Awesome -->         
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"> 
        <!-- Bootstrap core CSS -->         
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"> 
        <!-- Material Design Bootstrap -->         
        <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.11/css/mdb.min.css" rel="stylesheet"> --> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>         
    </head>     
    <body> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item"> 
                    <a href="index.jsp">Inicio</a> 
                </li>                 
                <li class="breadcrumb-item active text-white">Servicios Prestados por Cliente o Automotor</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <%            ReportesDAO rserv = new ReportesDAO();

            List<String> listacedulax = new LinkedList();
            List<String> listaplacax = new LinkedList();

            ServiciosPrestadosDAO daoServiPresta = new ServiciosPrestadosDAO();
            ClienteDAO dcli = new ClienteDAO();

            if (documento != null) {
                int doc = Integer.parseInt(documento);
                int idcliente = dcli.Consultarfkcliente(doc);
                listacedulax = rserv.ReporteCantidadxNumdoc(doc, idcliente);

            }
            if (placa != null) {

                listaplacax = rserv.ReporteCantidadxPlaca(placa);

            }


        %> 
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky text-justify"> 
                        <%@include file="menu.jsp" %>                        
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Servicios Prestados por Cliente o Automotor</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 

                            </div>                             

                        </div>                         
                    </div>                     
                    <div class="text-left bg-white pr-5 pb-5 pl-5 col-md-12"> 
                        <h4 class="mb-3 mt-auto">Reporte de Servicios Prestados</h4> 
                        <form class="needs-validation" novalidate="" method="post" action="ReporteServicioCV"> 
                            <div class="row"> 
                                <div class="col-md-6 mb-3 mt-2 col-lg-4">Seleccione el Tipo de Reporte
                                    <br> 
                                    <script>

                                        function tiposelect() {
                                            var option = document.getElementById("tipoReporte");
                                            var checks = document.getElementById('buttons1');
                                            var checks2 = document.getElementById('buttons2');


                                            if (option.value === "1") {

                                                checks.style.display = 'block';
                                            } else {
                                                checks.style.display = 'none';
                                            }

                                            if (option.value === "2") {

                                                checks2.style.display = 'block';
                                            } else {
                                                checks2.style.display = 'none';
                                            }


                                        }

                                    </script>                                     
                                    <select id="tipoReporte" onchange="tiposelect()" class="custom-select" name="tipoReporte"> 
                                        <option selected>Seleccione una opción</option>                                         
                                        <option value="1">Cantidad de Servicios por Cliente</option>                                         
                                        <option value="2">Cantidad de Servicios por Automotor</option>                                         
                                    </select>                                     
                                    <div class="invalid-feedback"> 
                                        Valid first name is required.
                                    </div>                                     
                                </div>                                 
                                <br>                                  
                                <div id="buttons1" class="mb-3 col-md-3 mt-2 ml-auto mr-auto col-lg-3" style="display: none" >Documento
                                    <div class="container-fluid"> 
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <select class="SelectDocumento" name="documento" style="width: 100%"> 
                                                </select>                                                 
                                            </div>                                             
                                        </div>                                         
                                    </div>                                     
                                    <script>
                                        var data = [];
                                        <% List<String> listaDocumentos = new LinkedList();
                                            ServicioDAO documentos = new ServicioDAO();
                                            listaDocumentos = documentos.obtenerDocumentos();

                                            for (int i = 0; i < listaDocumentos.size(); i++) {%>
                                        data[<%out.print(i);%>] = "<%out.print(listaDocumentos.get(i));%>";
                                        <%}%>

                                        var placeholder = "select";
                                        $(".SelectDocumento").select2({
                                            data: data,
                                            placeholder: placeholder,
                                            allowClear: false,
                                            minimumResultsForSearch: 1
                                        });
                                    </script>                                     
                                    <br> 
                                </div>                                 
                                <div id="buttons2" class="mb-3 col-md-3 mt-2 ml-auto mr-auto col-lg-3" style="display: none">Placa
                                    <div class="container-fluid"> 
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <select class="SelectPlaca" name="placa" style="width: 100%"> 
                                                </select>                                                 
                                            </div>                                             
                                        </div>                                         
                                    </div>                                     
                                    <script>
                                        var data = [];
                                        <% List<String> listaPlacas = new LinkedList();
                                            ServicioDAO placas = new ServicioDAO();
                                            listaPlacas = placas.obtenerPlacas();

                                            for (int i = 0; i < listaPlacas.size(); i++) {%>
                                        data[<%out.print(i);%>] = "<%out.print(listaPlacas.get(i));%>";
                                        <%}%>

                                        var placeholder = "select";
                                        $(".SelectPlaca").select2({
                                            data: data,
                                            placeholder: placeholder,
                                            allowClear: false,
                                            minimumResultsForSearch: 1
                                        });
                                    </script>                                     
                                    <br> 
                                </div>                                 
                            </div>                             
                            <div class="invalid-feedback"> 
                                Valid first name is required.
                            </div>                             
                    </div>
                    
                    <%List<Integer> lista2totalsuma = new LinkedList();%>
                    <%String divplaca = "";%>
                    <%String str1 = "";%>

                    <div class="row"> 
                    </div>                     
                    <hr class="mb-4"> 
                    <div class="row mb-5 "> 
                        <button class="btn btn-primary btn-lg btn-block m-auto col-md-4" type="submit">Consultar</button>                         
                    </div>                     
                    </form>                 
                    <%if (tiporeporte != null) {
                            if (tiporeporte.equals("1")) {%> 
                    <table style="width: 96%;float: left" class="table"> 
                        <thead> 
                            <tr> 
                                <th style="width: 12.2px">nombre</th> 
                                <th style="width: 12.2px">fechaservicio</th> 
                                <th style="width: 12.2px">placa</th> 
                                <th style="width: 12.2px">lavado</th> 
                                <th style="width: 12.2px">polichado</th> 
                                <th style="width: 12.2px">cambio de aceite</th> 
                                <th style="width: 12.2px">balanceo</th> 
                                <th style="width: 12.2px">alineacion</th> 
                                <th style="width: 12.2px">Total</th> 
                                <th style="width: 12.2px">servicios</th> 
                                
                            </tr>                             
                        </thead>                         
                    </table>                     
                    <%

                        int split = 8;
                        int partiplaca = 2;
                        String tipocosto="";
                        ReportesDAO repu=new ReportesDAO();

                        for (int j = 0; j < listacedulax.size(); j++) {%> 
                    <table style="width: 100px;float: left" class="table"> 
                        <tbody> 
                            <tr> 
                                <%if (j == partiplaca) {
                                        divplaca = listacedulax.get(j);
                                        tipocosto=repu.consultatipocosto(Integer.parseInt(documento), divplaca);
                                        partiplaca = partiplaca + 9;
                                    }
                                   
                                    if (j == split) {
                                        List<String> lista1 = new LinkedList();
                                        str1 = listacedulax.get(j);
                                        String[] parts = str1.split(",");
                                        for (int i = 0; i < parts.length; i++) {
                                            lista1.add(parts[i].trim());

                                        }
                                        //aca llamo mi consulta sumo
                                        int sumo=0;
                                        for (int k = 0; k < lista1.size(); k++) {
                                                int valSum=repu.consultacostoxServicio(tipocosto, lista1.get(k));
                                                sumo=sumo+valSum;
                                            }
                                        lista2totalsuma.add(sumo);%>
                                        <td style="height:100px"><%out.print(sumo);%></td>
                                        <%
                                        // crea una lista hace split y trim recorre haciendo una consulta mientras suma.. cada total en otra lista

                                        split = split + 9;
                                    }%>
                                <td style="height:100px"><%out.print(listacedulax.get(j));%></td> 

                            </tr>                                 
                        </tbody>                             
                    </table>                         
                    <%
                                }
                            }
                        }
                    %>
               
                    <%if (tiporeporte != null) {
                            if (tiporeporte.equals("2")) {%> 
                    <table style="width: 100%;float: left" class="table"> 
                        <thead> 
                            <tr> 
                                <th style="width: 10px">fechaservicio</th> 
                                <th style="width: 10px">placa</th> 
                                <th style="width: 10px">lavado</th> 
                                <th style="width: 10px">polichado</th> 
                                <th style="width: 10px">cambio de aceite</th> 
                                <th style="width: 10px">balanceo</th> 
                                <th style="width: 10px">alineacion</th> 
                                <th style="width: 10px">servicios</th>                                
                            </tr>                                 
                        </thead>                             
                    </table>   
                 
                    <%

                        for (int j = 0; j < listaplacax.size(); j++) {%> 
                    <table id="ejemplo<%out.print(j);%>" style="width: 125px;float: left" class="table"> 
                        <tbody> 
                            <tr> 
                                <td  style="height:100px"><%out.print(listaplacax.get(j));%></td> 
                                <% // 0 a 6 

                                %> 
                            </tr>                                     
                        </tbody>                                 
                    </table>                             
                    <%}%> 

                    <%--<table class="total table-bordered table-light"style="width: 100%"> 
                        <tr> 
                            <td class="table-dark"style="width:260px"> Total</td> 
                            <td style="width:130px"></td> 
                            <td style="width:120px"></td> 
                            <td style="width:130px"></td> 
                            <td style="width:120px"></td> 
                            <td s>5</td> 
                        </tr>                             
                    </table>  --%>

                    <% }
                        }
                    %> 

                    <script>
                        document.addEventListener("DOMContentLoaded", function (event) {


                            if (document.readyState !== "complete") {
                        <% tiporeporte = null;
                        %>
                            }

                        });
                        $(document).ready(function ()
                        {
                            
                            //Defino los totales de mis 2 columnas en 0
                            for (i = 2; i <= 7; i++)
                            {
                                var total1 = 0;
                                
                                //
                                //Recorro todos los tr ubicados en el tbody
                                $('#ejemplo' + i + ' tbody').find('tr').each(function (i, el) {

                                    //Voy incrementando las variables segun la fila ( .eq(0) representa la fila 1 )     
                                    total1 += parseFloat($(this).find('td').eq(0).text());
                                });
                                $('.total td:nth-child(' + i + ')').text(total1);
                            }
                        });
                    </script>
                    <%-- <table >
                        
                    <%for (int i = 0; i < lista2totalsuma.size(); i++) {%>
                    <tr style="height: 118px"><td><%out.print(lista2totalsuma.get(i));%></td></tr>
                            
                       <% }
                    %>
                    </table> --%>
            </div>             
        </main>         
    </div>     
    <!-- Bootstrap core JavaScript
            ================================================== -->     
    <!-- Placed at the end of the document so the pages load faster -->     
    <script src="assets/js/jquery.min.js"></script>     
    <script src="assets/js/popper.js"></script>     
    <script src="bootstrap/js/bootstrap.min.js"></script>     
    <!-- Icons -->     
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>     
    <script>feather.replace()</script>     
    <!-- Graphs -->     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>     
    <script>
                        var ctx = document.getElementById("myChart");
                        var myChart = new Chart(ctx, {
                            type: 'line',
                            data: {
                                labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                datasets: [{
                                        data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                        lineTension: 0,
                                        backgroundColor: 'transparent',
                                        borderColor: '#007bff',
                                        borderWidth: 4,
                                        pointBackgroundColor: '#007bff'
                                    }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                            ticks: {
                                                beginAtZero: false
                                            }
                                        }]
                                },
                                legend: {
                                    display: false,
                                }
                            }
                        });
    </script>
